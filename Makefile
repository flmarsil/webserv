CC 				= 	clang++

FLAGS 			= 	-Wall -Werror -Wextra -std=c++98

INCLUDES 		= 	$(addprefix -I webserv/, Webserv.hpp)

SRCS 			=	$(addprefix webserv/, main.cpp 				\
					socket/ClientSocket.cpp						\
					socket/ServerSocket.cpp						\
					server/HttpServer.cpp 						\
					server/HttpRequest.cpp 						\
					server/HttpRequestParser.cpp				\
					server/HttpRequestProcess.cpp 				\
					server/HttpResponse.cpp 					\
					parsing/ConfigFileParser.cpp				\
					parsing/ConfigServer.cpp					\
					utils/Debugguer.cpp 						\
					utils/Syntax.cpp							\
					utils/utils.cpp								\
					header/Header_Request.cpp					\
					header/Header_Parsing.cpp					\
					env/Env.cpp									\
					cgi/Cgi.cpp)								

OBJS 			= 	$(SRCS:.cpp=.o)

$(OBJS) 		= 	$(INCLUDES)

NAME 			= 	webserv.out

$(NAME)			: 	$(OBJS)
					@ $(CC) $(FLAGS) $(OBJS) -o $@

%.o : %.cpp
					@ $(CC) $(FLAGS) -c -o $@ $<

all 			: 	$(NAME)

clean 			:
					@ rm -f $(OBJS)

fclean 			: 	clean
					@ rm -f $(NAME)
					@ rm -f ./logs/*
					@ rm -rf ./.tmp
					@ rm -rf ./tmp
					@ rm -rf upload_default

re 				: 	fclean all
