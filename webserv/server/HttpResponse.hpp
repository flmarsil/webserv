#ifndef HTTPRESPONSE_HPP
#define HTTPRESPONSE_HPP

#include "../Webserv.hpp"

class HttpResponse
{
public:
    /* Aliases definition */
    typedef uint8_t     byte;    
    typedef std::map<std::string, std::string> map_ss;

	enum status_response_t {
		START,
		HEAD_SENT,
		RESPONSE_SENT,
        NONE
	};

public:
    /* Constructor */
    HttpResponse();
    /* Destructor */
    ~HttpResponse();
    /* Copy constructor */
    HttpResponse(HttpResponse& copy);
    /* Overloading operator */
    const HttpResponse& operator = (HttpResponse& assign);
    //std::string& operator += (const std::string& string);

    /* Methods */
    char *getResponse();

    // Setters
    void setStatusResponse(status_response_t status);
    void setStatusLine(const std::string& line);
    //void setUri(const std::string& uri);
    //void setContentType(const std::string& contentType);
    void setHeaders(const std::string& key, const std::string& value);
    void setBody(char *body);
    void setSizeOfBody(size_t size);
    void setLenghtResponse(size_t lenght);
    //void setLenghtSent(size_t lenght);
    void setIsCgiBody();
    void setCgiResponsePath(std::string path);
    void deleteBody();

    // Getters
    status_response_t getStatusResponse() const;
    const std::string& getSatusLine() const;
    //const std::string& getUri() const;
    //const std::string& getContentType() const;
    const map_ss& getHeaders() const;
    const std::string getStringHeader();
    char *getBody();
    size_t getSizeOfBody();
    bool getIsCgiBody();
    std::string getCgiResponsePath();
    //const std::string& getFinalBuffer() const;
    size_t getLenghtResponse() const;
    //size_t getLenghtSent() const;

private:
    /* Attributs */
	status_response_t	    _status;

    std::string             _statusLine;
    //std::string             _uri;
    //std::string             _contentType;
    map_ss                  _headers;
    char*       _body;
    size_t      _sizeOfBody;
    bool        _isCgiBody;
    std::string _cgiresponsePath;
    //std::string             _finalBuffer;

    size_t                  _lenghtResponse;
    //size_t                  _lenghtSent;


}; // class HttpResponse

#endif
