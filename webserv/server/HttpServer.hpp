#ifndef HTTPSERVER_HPP
#define HTTPSERVER_HPP

#include "../Webserv.hpp"

/*
    HttpServer est le serveur qui va être en attente des 
    connexions clients sur un port prédéfini.
    Pour chaque connexion reçue, il la stock dans une liste
    et recupère sa requête http. Il se charge ensuite d'envoyer
    la réponse http correspondante au client.
*/

#define WRITE 1
#define READ 0

class ClientSocket;
class ServerSocket;

class HttpServer
{
public:
    /* Aliases definition  */
    typedef int                 SOCKET;
    typedef struct sockaddr_in  SOCK_ADDR;
    typedef socklen_t           SOCK_LEN;
    typedef int                 PORT;
    
public:
    /* Constructor */
    HttpServer();
    /* Destructor */
    ~HttpServer();
    /* Copy constructor */
    HttpServer(const HttpServer& copy);
    /* Overloading operator */
    const HttpServer& operator = (const HttpServer& assign);
    /* Methods */
    int parsing(std::string& filepath);
    int setupServers();
    void start();

    std::list<ConfigServer> temporaryManualFillingConfigServer(int port); // a supprimer une fois le parsing terminé
    int parsingVerificationDebugger();  // a supprimer une fois le parsing terminé
    
    // Getters
    fd_set getReadFds() const;
    SOCKET getHightSocket() const;
    SOCKET getNbReadyFds() const;
    SOCKET getNewClientSocket() const;
    SOCK_ADDR getNewClientAddr() const;
    std::list<ServerSocket> getServerSocketHandler() const;
    std::list<ClientSocket> getClientSocketHandler() const;
    std::list<ConfigServer>& getConfigServer();
    const std::list<ConfigServer>& getConfigServer() const;
    std::ifstream& getConfigFile() const;

    /* Signal */
    static int signal;
    static void setSignal(int value);

private:
    /* Attributs */
    std::ifstream               _configFile;
    std::list<ConfigServer>     _configServer;          // contient les infos recuperés dans le fichier de configuration

    fd_set                      _fdSet[2];
    SOCKET                      _hightSocket;
    SOCKET                      _nbReadyFds;            // nombre de socket(fd) disponible à l'instant T pour traiter des demandes

    std::list<ServerSocket>     _ServerSocketHandler;   // contient toutes sockets en ecoute sur le serveur
    std::list<ClientSocket>     _ClientSocketHandler;   // contient toutes sockets gerant les connexions client en cours sur le serveur

    /* Private methods */
    void _loopLauncher();            
    void _connexionHandler();                           
    void _readingHandler();
    void _writingHandler();                            
    void _selectHandler();
    void _addNewServerSocket(ServerSocket& newServerSocket);
    // void _addNewClientSocket(SOCKET newClientSocket, PORT servPort, SOCK_ADDR newCliAddr);
    void _addNewClientSocket(SOCKET newSocket, SOCK_ADDR newCliAddr, const std::list<const ConfigServer*>& configServer, int port);
    void _addClientSocketToFdset(std::list<ClientSocket>& clientSocket);
    void _addServerSocketToFdset(std::list<ServerSocket>& serverSocket);

    template <typename T>
    void _closeAllSocket(std::list<T>& xSocketHandler) {
        typename std::list<T>::iterator it = xSocketHandler.begin();
        typename std::list<T>::iterator ite = xSocketHandler.end();
    
        for (; it != ite ; ++it) {
            close(it->getSocket());
        }
    }
};

#endif
