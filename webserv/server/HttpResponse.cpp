#include "../Webserv.hpp"

/* Constructor */
HttpResponse::HttpResponse()
    : _status(NONE),
      _headers(),
      _body(0),
      _isCgiBody(false),
      _lenghtResponse(0)
//  _lenghtSent(0)
{
}

/* Destructor */
HttpResponse::~HttpResponse() {}

/* Copy constructor */
HttpResponse::HttpResponse(HttpResponse &copy)
    : _status(copy.getStatusResponse()),
      _statusLine(copy.getSatusLine()),
      // _uri(copy.getUri()),
      //_contentType(copy.getContentType()),
      _headers(copy.getHeaders()),
      _body(copy.getBody()),
      _isCgiBody(copy.getIsCgiBody()),
      _lenghtResponse(copy.getLenghtResponse())
//_lenghtSent(copy.getLenghtSent())
{
}

/* Overloading operator */
const HttpResponse &HttpResponse::operator=(HttpResponse &assign)
{
    _status = assign.getStatusResponse();
    _statusLine = assign.getSatusLine();
    //_uri = assign.getUri();
    //_contentType = assign.getContentType();
    _headers = assign.getHeaders();
    _body = assign.getBody();
    _isCgiBody = assign.getIsCgiBody();
    _lenghtResponse = assign.getLenghtResponse();
    //_lenghtSent = assign.getLenghtSent();
    return (*this);
}

char *HttpResponse::getResponse()
{
    std::string header = getSatusLine() + "\n";
    header.append(getStringHeader());

    if (_isCgiBody)
    {
        std::streampos size = NULL;
        _body = NULL;
        _body = readBinaryFile(_cgiresponsePath, &size);
        _sizeOfBody = size;
    }

    if (_body)
    {
        _lenghtResponse += _sizeOfBody;

        size_t i = 0;
        if (_isCgiBody)
        {
            while (_body[i])
            {
                if (_body[i] == '\r' && _body[i + 1] == '\n' && _body[i + 2] == '\r' && _body[i + 3] == '\n')
                {
                    break;
                }
                i++;
            }
            header.append("Content-Length: " + to_string(_sizeOfBody - (i + 4)) + "\n");      
        }else{
            header.append("Content-Length: " + to_string(_sizeOfBody) + "\n");
        }
 
    }

    if (!_isCgiBody)
    {
        header.append("\r\n");
    }
    _lenghtResponse += header.size();
    // std::cout << header << std::endl;

    char *response = NULL;

    if (!(response = static_cast<char *>(malloc(sizeof(char) * (_lenghtResponse + 1)))))
        return (0);

    ///copy du header
    size_t i = 0;
    for (; i < header.size(); i++)
    {
        response[i] = header.at(i);
    }

    if (_body)
    {
        for (size_t y = 0; y < getSizeOfBody(); y++)
        {
            response[i] = _body[y];
            i++;
        }
    }

    return (response);
}

// std::string& HttpResponse::operator += (const std::string& string) {
//     return _finalBuffer += string;
// }

/* Methods */

// Setters
void HttpResponse::setStatusResponse(status_response_t status)
{
    _status = status;
}

void HttpResponse::setStatusLine(const std::string &line)
{
    _statusLine = line;
}

// void HttpResponse::setUri(const std::string& uri) {
//     _uri = uri;
// }

// void HttpResponse::setContentType(const std::string& contentType) {
//     _contentType = contentType;
// }

void HttpResponse::setHeaders(const std::string &key, const std::string &value)
{
    _headers[key] = value;
}

void HttpResponse::setBody(char *body)
{
    _body = body;
}

void HttpResponse::setSizeOfBody(size_t size)
{
    _sizeOfBody = size;
}

void HttpResponse::setLenghtResponse(size_t lenght)
{
    _lenghtResponse = lenght;
}

// void HttpResponse::setLenghtSent(size_t lenght) {
//     _lenghtSent = lenght;
// }

void HttpResponse::setIsCgiBody()
{
    _isCgiBody = true;
}

void HttpResponse::setCgiResponsePath(std::string path)
{
    _cgiresponsePath = path;
}

void HttpResponse::deleteBody()
{
    if (_body)
    {
       free(_body);
    }
}

// Getters
HttpResponse::status_response_t HttpResponse::getStatusResponse() const
{
    return _status;
}

const std::string &HttpResponse::getSatusLine() const
{
    return _statusLine;
}

// const std::string& HttpResponse::getUri() const {
//     return _uri;
// }

// const std::string& HttpResponse::getContentType() const {
//     return _contentType;
// }

const HttpResponse::map_ss &HttpResponse::getHeaders() const
{
    return _headers;
}

const std::string HttpResponse::getStringHeader()
{
    std::string str;

    for (std::map<std::string, std::string>::iterator it = this->_headers.begin(); it != this->_headers.end(); ++it)
    {
        str.append(it->first + ": " + it->second + "\n");
    }

    return (str);
}

char *HttpResponse::getBody()
{
    return _body;
}

size_t HttpResponse::getSizeOfBody()
{
    return _sizeOfBody;
}

bool HttpResponse::getIsCgiBody()
{
    return (_isCgiBody);
}

std::string HttpResponse::getCgiResponsePath()
{
    return _cgiresponsePath;
}

// const std::string& HttpResponse::getFinalBuffer() const {
//     return _finalBuffer;
// }

size_t HttpResponse::getLenghtResponse() const
{
    return _lenghtResponse;
}

// size_t HttpResponse::getLenghtSent() const {
//     return _lenghtSent;
// }
