#include "../Webserv.hpp"

/* Constructor */
HttpRequest::HttpRequest() 
    :   _status(NONE),
        _rawRequest(0),
        _tmpBodyFile(0),
        _bodyExpected(false),
        _bodySizeExpected(0),
        _bodyReceived(false),
        _bodySizeReceived(0) {}

/* Destructor */
HttpRequest::~HttpRequest() {}

/* Copy constructor */
HttpRequest::HttpRequest(const HttpRequest& copy)
    :   _status(copy.getStatusRequest()),
        _rawRequest(copy.getRawRequest()),
        _tmpBodyFile(copy.getBodyFile()),
        _bodyExpected(copy.getBodyIsExpected()),
        _bodySizeExpected(copy.getBodySizeExpected()),
        _bodyReceived(copy.getBodyIsReceived()),
        _bodySizeReceived(copy.getBodySizeReceived()) {}

/* Overloading operator */
const HttpRequest& HttpRequest::operator = (const HttpRequest& assign) {
    _status = assign.getStatusRequest();
    _rawRequest = assign.getRawRequest();
    _tmpBodyFile = assign.getBodyFile();
    _bodyExpected = assign.getBodyIsExpected();
    _bodySizeExpected = assign.getBodySizeExpected();
    _bodyReceived = assign.getBodyIsReceived();
    _bodySizeReceived = assign.getBodySizeReceived();
    return (*this);
}

/* Methods */

// Setters
void HttpRequest::setResolvedPath(std::string path){
    _resolvedPath = path;
}

void HttpRequest::setStatusRequest(status_request_t status) {
    _status = status;
}

void HttpRequest::setBodyIsExpected() {
    _bodyExpected = true;
}

void HttpRequest::setBodyIsReceived() {
    _bodyReceived = true;
}

void HttpRequest::setBodySizeExpected(size_t size) {
    _bodySizeExpected = size;
}

// void HttpRequest::setTmpFile() {
//     std::vector<byte>::iterator it = _rawRequest.begin();
//     std::vector<byte>::iterator ite = _rawRequest.end();

//     for (; it != ite ; ++it) {
//         std::cout << GREEN << *it << " ";
//     }
//     std::cout << std::endl; 
// }

void HttpRequest::setHeader_request(Header_request header) {
    _requestHeaders = header;
}

void HttpRequest::setBodySizeReceived(size_t size) {
    _bodySizeReceived = size;
}

// Getters
HttpRequest::status_request_t HttpRequest::getStatusRequest() const {
    return _status;
}

const std::vector<HttpRequest::byte>& HttpRequest::getRawRequest() const {
    return _rawRequest;
}

std::vector<HttpRequest::byte>& HttpRequest::getRawRequest() {
    return _rawRequest;
}

int HttpRequest::getBodyFile() {
    return (_tmpBodyFile);
}

const int& HttpRequest::getBodyFile() const {
    return (_tmpBodyFile);
}

size_t HttpRequest::getBodySizeExpected() const {
    return _bodySizeExpected;
}

bool HttpRequest::getBodyIsExpected() const {
    return _bodyExpected;
}

bool HttpRequest::getBodyIsReceived() const {
    return _bodyReceived;
}

std::string HttpRequest::getResolvedPath() const {
    return _resolvedPath;
}

size_t HttpRequest::getBodySizeReceived() const {
    return _bodySizeReceived;
}

size_t HttpRequest::getBodySizeReceived() {
    return _bodySizeReceived;
}

Header_request &HttpRequest::getHeaders() {
    return _requestHeaders;
}

const Header_request& HttpRequest::getHeaders() const {
    return _requestHeaders;
}


