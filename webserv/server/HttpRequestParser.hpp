/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HttpRequestParser.hpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/10 12:16:26 by fassani           #+#    #+#             */
/*   Updated: 2021/05/26 17:40:55 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HTTPREQUESTPARSER_HPP
#define HTTPREQUESTPARSER_HPP

#include "../Webserv.hpp"

class HttpRequestParser
{
    public:
    static void parsing(HttpRequest &request, const ConfigServer *conf, size_t length);
};

#endif