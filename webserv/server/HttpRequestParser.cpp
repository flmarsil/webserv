/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HttpRequestParser.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/10 12:16:21 by fassani           #+#    #+#             */
/*   Updated: 2021/06/09 14:55:39 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./HttpRequestParser.hpp"
#include <ios>
#include <ostream>
#include <ctime>

static std::vector<uint8_t>::iterator find_end_of_Header(std::vector<uint8_t> &rawRequest)
{
    std::vector<uint8_t>::iterator it = rawRequest.begin();
    std::vector<uint8_t>::iterator ite = rawRequest.end();

    for (; it != ite; ++it)
    {
        std::string str(it, it + 4);
        if (str == "\r\n\r\n")
        {
            // std::cout << RED << "fin du header trouvé" << RESET << std::endl;
            return (it);
        }
    }
    return (it);
}

static double pourcentage(double length, double length_limite)
{
    return ((length / length_limite) * 100);
}

static void body_to_file(const std::vector<uint8_t> &rawRequest, std::string path)
{
    mkdir("./.tmp", 0777);

    std::ofstream file;
    file.open(path.c_str(), std::ofstream::out | std::ofstream::binary | std::ofstream::app);
    if (file.is_open())
    {
        for (size_t i = 0; i < rawRequest.size(); ++i)
            file << rawRequest[i];
    }
    file.close();
}

void HttpRequestParser::parsing(HttpRequest &request, const ConfigServer *conf, size_t length)
{

    if (request.getStatusRequest() < HttpRequest::HEADERS_RECEIVED)
    {
        std::vector<uint8_t>::iterator it_end_header = find_end_of_Header(request.getRawRequest()); //trouve la fin du header
        if (it_end_header != request.getRawRequest().end())
        {
            std::string str(request.getRawRequest().begin(), it_end_header); //convertit le vector<uint8_t> en un string

            if (str.size() > MAX_HEADER_LEN)
            {
                request.getHeaders().setHeader_length(str.size());
                request.getRawRequest().clear();
                request.setStatusRequest(HttpRequest::REQUEST_RECEIVED);
                return;
            }

            Header_request tmp_header(str);
            tmp_header.setBody_tmp_path("./.tmp/body_request_" + generate_random_tmp_path());
            createtmpfile(tmp_header.getBody_tmp_path());
            request.setHeader_request(tmp_header);
            request.setStatusRequest(HttpRequest::HEADERS_RECEIVED);

            // std::cout << BLUE << "\nMethod: [" + request.getHeaders().getRequest().Method + "]" << RESET << std::endl;
            // std::cout << BLUE << "Path: [" + request.getHeaders().getRequest().Path + "]" << RESET << std::endl;
            // std::cout << BLUE << "Query: [" + request.getHeaders().getRequest().Query + "]" << RESET << std::endl;
            // std::cout << BLUE << "Version: [" + request.getHeaders().getRequest().Version + "]" << RESET << std::endl;

            if ((request.getHeaders().getRequest().Path.size() + request.getHeaders().getRequest().Query.size()) > MAX_URI_LEN)
            {
                request.getRawRequest().clear();
                request.setStatusRequest(HttpRequest::REQUEST_RECEIVED);
            }

            if (request.getHeaders().getContent_Length() > 0)
            {
                // std::cout << RED << "Content expected, length : " << request.getHeaders().getContent_Length() << RESET << std::endl;
                request.setBodyIsExpected();
                request.setBodySizeExpected(request.getHeaders().getContent_Length());

                if (HttpRequestProcess::isSizeOverload(conf, request))
                {
                    request.getRawRequest().clear();
                    request.setStatusRequest(HttpRequest::REQUEST_RECEIVED);
                }
            }
            else
            {
                request.getRawRequest().clear();
                request.setStatusRequest(HttpRequest::REQUEST_RECEIVED);
            }
        }
    }

    if (request.getBodyIsExpected() && (request.getStatusRequest() < HttpRequest::BODY_RECEIVED))
    {
        if (request.getBodySizeReceived() == 0)
        {
            //supprime le header dans rawRequest
            request.getRawRequest().erase(request.getRawRequest().begin(), request.getRawRequest().begin() + request.getHeaders().getHeader_length());
            length -= request.getHeaders().getHeader_length();
        }

        //ecrit le body dans un fichier dans ./tmp/
        body_to_file(request.getRawRequest(), request.getHeaders().getBody_tmp_path());
        request.getRawRequest().clear();

        request.setBodySizeReceived(request.getBodySizeReceived() + length);

        request.getHeaders().setContent_Length(request.getBodySizeReceived());
        
        //std::cout << request.getBodySizeReceived() << "/" << request.getBodySizeExpected() << std::endl;
        // std::cout << GREEN << "Download body in " + request.getHeaders().getBody_tmp_path() + " : " << pourcentage(request.getBodySizeReceived(), request.getBodySizeExpected()) << "%" << RESET << std::endl;
        std::cout << GREEN << "Download body : " << pourcentage(request.getBodySizeReceived(), request.getBodySizeExpected()) << "%" << RESET << std::endl;

        if (request.getBodySizeReceived() == request.getBodySizeExpected())
        {
            request.getRawRequest().clear();
            request.setStatusRequest(HttpRequest::REQUEST_RECEIVED);
            DEBUG_COUT("Téléchargement terminé");
        }
    }
}