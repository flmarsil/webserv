/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HttpRequestProcess.hpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/22 12:38:29 by fassani           #+#    #+#             */
/*   Updated: 2021/06/03 17:44:46 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HttpRequestProcess_HPP
#define HttpRequestProcess_HPP

#include "../Webserv.hpp"
#include <sys/types.h>
#include <dirent.h>

//   static void (*method_func[9])(ClientSocket &clientsocket) = {
//     HttpRequestProcess::method_GET,
//     HttpRequestProcess::method_HEAD,
//     HttpRequestProcess::method_POST,
//     HttpRequestProcess::method_PUT,
//     HttpRequestProcess::method_DELETE,
//     HttpRequestProcess::method_CONNECT,
//     HttpRequestProcess::method_OPTIONS,
//     HttpRequestProcess::method_TRACE,
//     HttpRequestProcess::method_PATCH};

class HttpRequestProcess
{
public:
    static const ConfigServer *getConf(int port , const std::list<const ConfigServer*>& configServer);
    // static std::string getDefaultFile(const ConfigServer *conf, std::string root, std::string location);
        static std::string getDefaultFile(const ConfigServer *conf, std::string location);
    static std::string::size_type getIndexPathInfo(std::string path);
    static const std::string resolvePath(const ConfigServer *conf, HttpRequest &request, std::string location);
    static const ConfigServer::Location getPathLocationBloc(const ConfigServer *conf, std::string &location);
    static const std::list<std::string> getLocationMethods(const ConfigServer *conf, std::string location);
    static const std::string isExecutedByCgi(const ConfigServer *conf, HttpRequest &request, std::string path);
    static bool isSizeOverload(const ConfigServer *conf, HttpRequest &request);
    static bool isAutoIndex(const ConfigServer *conf, HttpRequest &request);
    static char *getAutoIndex(std::string resolvedPath, std::string unResolvedPath, size_t *size);
    static std::string getUploadStore(const ConfigServer *conf, HttpRequest &request);
    static bool methodChecker(const ConfigServer *conf, HttpRequest &request);
    static void setError(const ConfigServer *conf, HttpResponse &response, std::string path, const status_code_t error);

    static void method_GET(ClientSocket &clientsocket);
    static void method_HEAD(ClientSocket &clientsocket);
    static void method_POST(ClientSocket &clientsocket);
    static void method_PUT(ClientSocket &clientsocket);
    static void method_DELETE(ClientSocket &clientsocket);
    static void method_CONNECT(ClientSocket &clientsocket);
    static void method_OPTIONS(ClientSocket &clientsocket);
    static void method_TRACE(ClientSocket &clientsocket);
    static void method_PATCH(ClientSocket &clientsocket);

    static const std::string method_list[];
    static const std::string getExtensionContentType(std::string extension);
};

#endif