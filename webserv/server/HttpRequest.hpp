#ifndef HTTPREQUEST_HPP
#define HTTPREQUEST_HPP


#include "../Webserv.hpp"

class HttpRequest
{
public:
    /* Aliases definition */
    typedef uint8_t     byte;    
    typedef int         FD;

    enum status_request_t {
        START,
        REQUEST_LINE_RECEIVED,
        HEADERS_RECEIVED,
        BODY_RECEIVED,
        REQUEST_RECEIVED,
        REQUEST_PROCESSED,
        NONE
    };

public:
    /* Constructor */
    HttpRequest();
    /* Destructor */
    ~HttpRequest();
    /* Copy constructor */
    HttpRequest(const HttpRequest& copy);
    /* Overloading operator */
    const HttpRequest& operator = (const HttpRequest& assign);
    /* Methods */

    // Setters
    void setResolvedPath(std::string path);
    void setStatusRequest(status_request_t status);
    void setBodyIsExpected();
    void setBodyIsReceived();
    void setBodySizeExpected(size_t size);
    // void setTmpFile();
    void setHeader_request(Header_request header);
    void setBodySizeReceived(size_t size);

    // Getters
    std::vector<byte>&          getRawRequest();
    int                         getBodyFile();
    Header_request&             getHeaders();
    size_t                      getBodySizeReceived();

    const Header_request&       getHeaders()            const;
    const std::vector<byte>&    getRawRequest()         const;
    status_request_t            getStatusRequest()      const;
    const int&                  getBodyFile()           const;
    size_t                      getBodySizeExpected()   const;
    bool                        getBodyIsExpected()     const;
    size_t                      getBodySizeReceived()   const;
    bool                        getBodyIsReceived()     const;
    std::string                 getResolvedPath()       const;

private:
    /* Attributs */
	status_request_t	    _status;

    std::vector<byte>       _rawRequest;
    Header_request          _requestHeaders;
    FD                      _tmpBodyFile;

    std::string             _resolvedPath;
    bool                    _bodyExpected;
    size_t                  _bodySizeExpected;
    bool                    _bodyReceived;
    size_t                  _bodySizeReceived;
};

#endif
