/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HttpRequestProcess.cpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/22 12:38:21 by fassani           #+#    #+#             */
/*   Updated: 2021/06/09 14:49:25 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HttpRequestProcess.hpp"

const ConfigServer *HttpRequestProcess::getConf(int port, const std::list<const ConfigServer *> &configServer)
{
    std::list<const ConfigServer *>::const_iterator it = configServer.begin();
    std::list<const ConfigServer *>::const_iterator ite = configServer.end();

    while (it != ite)
    {
        if (((*it))->getPort() == port)
            return (*it);
        it++;
    }
    return (configServer.front());
}

std::string HttpRequestProcess::getDefaultFile(const ConfigServer *conf, std::string location)
{
    if (location.empty() || !conf)
        return ("");

    std::string locationReturn = location;
    ConfigServer::Location locationBloc = getPathLocationBloc(conf, locationReturn);
    
    std::string root = locationBloc.getRoot();
    if (root.at(root.size() - 1) == '/')
        root.resize(root.size() - 1);

    std::string resolvedPath = root + location.substr(locationReturn.size());
    

    std::list<std::string>::iterator it = locationBloc.getIndex().begin();
    std::list<std::string>::iterator ite = locationBloc.getIndex().end();

    while (it != ite)
    {
        if (resolvedPath.at(resolvedPath.size() -1) != '/')
            resolvedPath.append("/");

        int retCode = fileExist(resolvedPath + *it);

        if (retCode == 1)
            return (resolvedPath + *it);
        it++;
    }

    return (root);
}

std::string::size_type HttpRequestProcess::getIndexPathInfo(std::string path)
{
    std::string::size_type position;
    position = path.rfind('.');

    if (position == std::string::npos)
        return (std::string::npos);

    std::string::size_type lpost = position;
    position = path.find('/', lpost);

    return (position);
}

const std::string HttpRequestProcess::resolvePath(const ConfigServer *conf, HttpRequest &request, std::string location)
{
    std::string::size_type posInfo = getIndexPathInfo(location);

    if (posInfo != std::string::npos)
    {
        std::string path_info = location.substr(posInfo + 1);
        if (!path_info.empty())
        {
            request.getHeaders().setRequest(request.getHeaders().getRequest().Method,
                                            location.substr(0, posInfo), request.getHeaders().getRequest().Query,
                                            path_info, request.getHeaders().getRequest().Version);
            location = request.getHeaders().getRequest().Path;
        }
    }

    if (location.empty() || !conf)
        return ("");

    std::string locationReturn = location;
    ConfigServer::Location locationBloc = getPathLocationBloc(conf, locationReturn);
    std::string locationTmp = "/";
    if (locationBloc.getRoot().empty())
        locationBloc = getPathLocationBloc(conf, locationTmp);


    std::string str;
    if( request.getHeaders().getRequest().Method == "POST")
    {
        if (locationBloc.getUploadStore().empty())
        {
            str.append("./upload_default");
        }else{
            str.append(locationBloc.getUploadStore());
        }

    }else{
        str.append(locationBloc.getRoot());
    }

    if (str.at(str.size() - 1) == '/')
        str.resize(str.size() - 1);

    std::string resolvedPath = str + location.substr(locationReturn.size());

    int retCode = fileExist(resolvedPath);

    if (retCode == 2) //si c'est un dossier
    {
        std::string temp_str = getDefaultFile(conf, location);
        if (str != temp_str)
            return (temp_str);
    }
    return (resolvedPath);
}

const ConfigServer::Location HttpRequestProcess::getPathLocationBloc(const ConfigServer *conf, std::string &location)
{
    if (location.empty() || !conf)
        return (conf->getLocationBlocs().back());
    std::list<ConfigServer::Location>::const_iterator it = conf->getLocationBlocs().begin();
    std::list<ConfigServer::Location>::const_iterator ite = conf->getLocationBlocs().end();

    if (location.at(location.size() - 1) == '/')
        location.resize(location.size() - 1);

    while (it != ite)
    {
        std::string str = it->getPath();
        if (str.size() > 1)
        {
            if (str.at(str.size() - 1) == '/')
                str.resize(str.size() - 1);
        }
        if (str == location)
            return (*it);
        it++;
    }

    std::string::size_type position;
    position = location.rfind('/');

    if (position == std::string::npos)
        return (conf->getLocationBlocs().back());

    std::string buffer = location.substr(0, position);
    const ConfigServer::Location monbloc = getPathLocationBloc(conf, buffer);
    location = buffer;
    return (monbloc);
}

const std::list<std::string> HttpRequestProcess::getLocationMethods(const ConfigServer *conf, std::string location)
{
    if (location.empty() || !conf)
        return (std::list<std::string>());

    ConfigServer::Location locationBloc = getPathLocationBloc(conf, location);

    if (locationBloc.getMethods().empty())
        return (std::list<std::string>());

    return (locationBloc.getMethods());
}

bool HttpRequestProcess::methodChecker(const ConfigServer *conf, HttpRequest &request)
{
    std::string requestMethod = request.getHeaders().getRequest().Method;
    std::string requestPath = request.getHeaders().getRequest().Path;

    std::list<std::string> methodsList = getLocationMethods(conf, requestPath);

    std::list<std::string>::const_iterator it = methodsList.begin();
    std::list<std::string>::const_iterator ite = methodsList.end();
    while (it != ite)
    {
        if (*it == requestMethod)
            return (true);
        it++;
    }
    return (false);
}

const std::string HttpRequestProcess::isExecutedByCgi(const ConfigServer *conf, HttpRequest &request, std::string path)
{
    std::string location = request.getHeaders().getRequest().Path;
    ConfigServer::Location locationBloc = getPathLocationBloc(conf, location);

    if (locationBloc.getCGIextention().empty())
        return ("");
    if (locationBloc.getCGIextention() == "*." + get_Path_Extention(path))
        return (locationBloc.getCGIpath());
    return ("");
}

bool HttpRequestProcess::isSizeOverload(const ConfigServer *conf, HttpRequest &request)
{
    std::string location = request.getHeaders().getRequest().Path;
    ConfigServer::Location locationBloc = getPathLocationBloc(conf, location);
    if (locationBloc.getClientMaxBodySize() < request.getHeaders().getContent_Length())
        return (true);
    return (false);
}

bool HttpRequestProcess::isAutoIndex(const ConfigServer *conf, HttpRequest &request)
{
    std::string location = request.getHeaders().getRequest().Path;
    ConfigServer::Location locationBloc = getPathLocationBloc(conf, location);
    return (locationBloc.getAutoIndex());
}

char *HttpRequestProcess::getAutoIndex(std::string resolvedPath, std::string unResolvedPath, size_t *size)
{
    *size = 0;
    if (resolvedPath.empty())
        return (0);

    if (resolvedPath.at(resolvedPath.size() - 1) != '/')
        resolvedPath.append("/");
    if (unResolvedPath.at(unResolvedPath.size() - 1) != '/')
        unResolvedPath.append("/");

    std::string top = "<html>\n<head>\n<title>Webserv fassani & flmarsil</title>\n</head>\n\n<body bgcolor=\"white\">\n<h1>Index of " + unResolvedPath + "</h1>\n<hr>\n<pre>\n";
    std::string bottom = "</pre>\n<hr>\n</body>\n</html>";

    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(resolvedPath.c_str())) != NULL)
    {
        top.append("<a href=\"../\">../</a>\n");
        while ((ent = readdir(dir)) != NULL)
        {
            if (to_string(ent->d_name) != "." && to_string(ent->d_name) != "..")
            {
                int retCode = fileExist(resolvedPath + ent->d_name);
                if (retCode == 1 || retCode == 3) //fichier ou symbolic link
                {
                    top.append("<a href=\"" + unResolvedPath + to_string(ent->d_name) + "\">" + to_string(ent->d_name) + "</a>\n");
                }
                else if (retCode == 2) //dossier
                {
                    top.append("<a href=\"" + unResolvedPath + to_string(ent->d_name) + "/\">" + to_string(ent->d_name) + "/</a>\n");
                }
            }
        }
        top.append(bottom);
        closedir(dir);
    }
    else
    {
        /* could not open directory */
        std::cerr << "error AutoIndex : impossible d'ouvrir le dossier : " + resolvedPath << std::endl;
        return (0);
    }

    char *response = NULL;

    if (!(response = static_cast<char *>(malloc(sizeof(char) * (top.size() + 1)))))
        return (0);

    for (size_t i = 0; i < top.size(); i++)
    {
        response[i] = top.at(i);
    }

    *size = top.size();

    return (response);
}

std::string HttpRequestProcess::getUploadStore(const ConfigServer *conf, HttpRequest &request)
{
    std::string location = request.getHeaders().getRequest().Path;
    ConfigServer::Location locationBloc = getPathLocationBloc(conf, location);
    return (locationBloc.getUploadStore());
}

void HttpRequestProcess::setError(const ConfigServer *conf, HttpResponse &response, std::string path, const status_code_t error)
{
    ConfigServer::Location locationBloc = getPathLocationBloc(conf, path);
    response.setStatusLine("HTTP/1.1 " + Syntax::status_codes_tab[error].code_str + " " + Syntax::status_codes_tab[error].reason_phrase);

    std::string errorPath = conf->getErrorPagePath();

    if (errorPath.empty())
        return;

    if (errorPath.at(errorPath.size() - 1) != '/')
        errorPath.append("/");

    response.setHeaders("Date", getCurrentDate());
    response.setHeaders("Server", "WebServ/1.0");

    std::list<status_code_t>::const_iterator it = conf->getErrorPageCodes().begin();
    std::list<status_code_t>::const_iterator ite = conf->getErrorPageCodes().end();

    while (it != ite)
    {
        if (*it == error)
        {
            std::streampos size = NULL;
            response.setBody(NULL);
            response.setBody(readBinaryFile(errorPath + Syntax::status_codes_tab[error].code_str + ".html", &size));
            response.setSizeOfBody(size);

            if (size != 0)
            {
                response.setHeaders("Content-Type", "text/html; charset=UTF-8");
                return;
            }
        }
        it++;
    }

    std::string default_error = "<!DOCTYPE html><html> <head> <style> *{ transition: all 0.6s; } html { height: 100%; } body{ font-family: 'Lato', sans-serif; color: #888; margin: 0; } #main{ display: table; width: 100%; height: 100vh; text-align: center; } .fof{ display: table-cell; vertical-align: middle; } .fof h1{ font-size: 50px; display: inline-block; padding-right: 12px; animation: type .5s alternate infinite; } .fof h2{ font-size: 50px; padding-right: 12px; animation: type .5s alternate infinite; } @keyframes type{ from{box-shadow: inset -3px 0px 0px #888;} to{box-shadow: inset -3px 0px 0px transparent;} } </style> </head> <body> <div id=\"projet\"> <div class=\"fof\"> <h2>WebServ 42project fassani & flmarsil</h2> </div> </div> <div id=\"main\"> <div class=\"fof\"> <h1>une erreur est survenue</h1> </div> </div> </body></html>";
    char *body = NULL;
    if (!(body = static_cast<char *>(malloc(sizeof(char) * default_error.size()))))
        return;

    for (size_t i = 0; i < default_error.size(); i++)
    {
        body[i] = default_error.at(i);
    }

    response.setBody(NULL);
    response.setBody(body);
    response.setSizeOfBody(default_error.size());
    response.setHeaders("Content-Type", "text/html; charset=UTF-8");
}

const std::string HttpRequestProcess::method_list[] = {
    "GET",
    "HEAD",
    "POST",
    "PUT",
    "DELETE",
    "CONNECT",
    "OPTIONS",
    "TRACE",
    "PATCH"};

const std::string HttpRequestProcess::getExtensionContentType(std::string extension)
{
    if (extension.at(0) != '.')
        extension.insert(0, ".");

    std::transform(extension.begin(), extension.end(), extension.begin(), ::tolower);

    //je sais que c'est moche, mais flemme..., ça fonctionne c'est le plus important non ? :D
    if (extension == ".aac")
        return ("audio/aac");
    else if (extension == ".abw")
        return ("application/x-abiword");
    else if (extension == ".arc")
        return ("application/octet-stream");
    else if (extension == ".avi")
        return ("video/x-msvideo");
    else if (extension == ".azw")
        return ("application/vnd.amazon.ebook");
    else if (extension == ".bin")
        return ("application/octet-stream");
    else if (extension == ".bmp")
        return ("image/bmp");
    else if (extension == ".bz")
        return ("application/x-bzip");
    else if (extension == ".bz2")
        return ("application/x-bzip2");
    else if (extension == ".csh")
        return ("application/x-csh");
    else if (extension == ".css")
        return ("text/css");
    else if (extension == ".csv")
        return ("text/csv");
    else if (extension == ".txt")
        return ("text/plain");
    else if (extension == ".doc")
        return ("application/msword");
    else if (extension == ".docx")
        return ("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    else if (extension == ".eot")
        return ("application/vnd.ms-fontobject");
    else if (extension == ".epub")
        return ("application/epub+zip");
    else if (extension == ".gif")
        return ("image/gif");
    else if (extension == ".htm")
        return ("text/html");
    else if (extension == ".html")
        return ("text/html");
    else if (extension == ".ico")
        return ("image/x-icon");
    else if (extension == ".ics")
        return ("text/calendar");
    else if (extension == ".jar")
        return ("application/java-archive");
    else if (extension == ".jpeg")
        return ("image/jpeg");
    else if (extension == ".jpg")
        return ("image/jpeg");
    else if (extension == ".js")
        return ("application/javascript");
    else if (extension == ".json")
        return ("application/json");
    else if (extension == ".mid")
        return ("audio/midi");
    else if (extension == ".midi")
        return ("audio/midi");
    else if (extension == ".mpeg")
        return ("video/mpeg");
    else if (extension == ".mpkg")
        return ("application/vnd.apple.installer+xml");
    else if (extension == ".odp")
        return ("application/vnd.oasis.opendocument.presentation");
    else if (extension == ".ods")
        return ("application/vnd.oasis.opendocument.spreadsheet");
    else if (extension == ".odt")
        return ("application/vnd.oasis.opendocument.text");
    else if (extension == ".oga")
        return ("audio/ogg");
    else if (extension == ".ogv")
        return ("video/ogg");
    else if (extension == ".ogx")
        return ("application/ogg");
    else if (extension == ".otf")
        return ("font/otf");
    else if (extension == ".png")
        return ("image/png");
    else if (extension == ".pdf")
        return ("application/pdf");
    else if (extension == ".ppt")
        return ("application/vnd.ms-powerpoint");
    else if (extension == ".pptx")
        return ("application/vnd.openxmlformats-officedocument.presentationml.presentation");
    else if (extension == ".rar")
        return ("application/x-rar-compressed");
    else if (extension == ".rtf")
        return ("application/rtf");
    else if (extension == ".sh")
        return ("application/x-sh");
    else if (extension == ".svg")
        return ("image/svg+xml");
    else if (extension == ".swf")
        return ("application/x-shockwave-flash");
    else if (extension == ".tar")
        return ("application/x-tar");
    else if (extension == ".tif")
        return ("image/tiff");
    else if (extension == ".tiff")
        return ("image/tiff");
    else if (extension == ".ts")
        return ("application/typescript");
    else if (extension == ".ttf")
        return ("font/ttf");
    else if (extension == ".vsd")
        return ("application/vnd.visio");
    else if (extension == ".wav")
        return ("audio/x-wav");
    else if (extension == ".weba")
        return ("audio/webm");
    else if (extension == ".webm")
        return ("video/webm");
    else if (extension == ".webp")
        return ("image/webp");
    else if (extension == ".woff")
        return ("font/woff");
    else if (extension == ".woff2")
        return ("font/woff2");
    else if (extension == ".xhtml")
        return ("application/xhtml+xml");
    else if (extension == ".xls")
        return ("application/vnd.ms-excel");
    else if (extension == ".xlsx")
        return ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    else if (extension == ".xml")
        return ("application/xml");
    else if (extension == ".xul")
        return ("application/vnd.mozilla.xul+xml");
    else if (extension == ".zip")
        return ("application/zip");
    else if (extension == ".3gp")
        return ("video/3gpp");
    else if (extension == ".3g2")
        return ("video/3gpp2");
    else if (extension == ".7z")
        return ("application/x-7z-compressed");
    else
        return ("application/octet-stream");
}

void HttpRequestProcess::method_GET(ClientSocket &clientsocket)
{
    DEBUG_COUT("Process Method GET");

    std::streampos size = NULL;
    clientsocket.getServResponse().setBody(NULL);
    clientsocket.getServResponse().setBody(readBinaryFile(clientsocket.getHttpRequest().getResolvedPath(), &size));
    if (fileExist(clientsocket.getHttpRequest().getResolvedPath()) == 1)
    {
        clientsocket.getServResponse().setSizeOfBody(size);
        clientsocket.getServResponse().setHeaders("Content-Type", getExtensionContentType(get_Path_Extention(clientsocket.getHttpRequest().getResolvedPath())));
        clientsocket.getServResponse().setHeaders("Server", "WebServ/1.0");
        clientsocket.getServResponse().setHeaders("Date", getCurrentDate());
        clientsocket.getServResponse().setHeaders("Last-Modified", getPathLastModifiedDate(clientsocket.getHttpRequest().getResolvedPath()));
        clientsocket.getServResponse().setStatusLine("HTTP/1.1 " + Syntax::status_codes_tab[OK].code_str + " " + Syntax::status_codes_tab[OK].reason_phrase);
    }
    else
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()),
                                     clientsocket.getServResponse(),
                                     clientsocket.getHttpRequest().getHeaders().getRequest().Path, INTERNAL_SERVER_ERROR);
        return;
    }
}

void HttpRequestProcess::method_HEAD(ClientSocket &clientsocket)
{
    (void)clientsocket;
    DEBUG_COUT("Process Method HEAD");
    HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()), clientsocket.getServResponse(),
                                 clientsocket.getHttpRequest().getHeaders().getRequest().Path, NOT_IMPLEMENTED);
}

void HttpRequestProcess::method_POST(ClientSocket &clientsocket)
{
    DEBUG_COUT("Process Method POST");
    std::string upload_dir = HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ())->getLocationBlocs().back().getUploadStore();
    if (upload_dir.empty())
    {
        mkdir("./upload_default", 0777);
        upload_dir = "./upload_default";
    }
    if (upload_dir.at(upload_dir.size() - 1) == '/')
        upload_dir.resize(upload_dir.size() - 1);

    // std::string path = upload_dir + clientsocket.getHttpRequest().getHeaders().getRequest().Path;
    std::string path = clientsocket.getHttpRequest().getResolvedPath();
    // std::cout << RED << path << RESET << std::endl;
    std::string pathDir = getPathFileDir(path);
    if (fileExist(pathDir) != 2)
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()), clientsocket.getServResponse(),
                                     clientsocket.getHttpRequest().getHeaders().getRequest().Path, INTERNAL_SERVER_ERROR);
        return;
    }

    if (getPathFileName(path) == "")
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()), clientsocket.getServResponse(),
                                     clientsocket.getHttpRequest().getHeaders().getRequest().Path, INTERNAL_SERVER_ERROR);
        return;
    }

    std::ifstream in(clientsocket.getHttpRequest().getHeaders().getBody_tmp_path().c_str(), std::ios::in | std::ios::binary);
    
    std::ofstream out(path.c_str(), std::ios::out | std::ios::binary);
    out << in.rdbuf();
    
    std::remove(clientsocket.getHttpRequest().getHeaders().getBody_tmp_path().c_str());

    clientsocket.getServResponse().setStatusLine("HTTP/1.1 " + Syntax::status_codes_tab[CREATED].code_str + " " + Syntax::status_codes_tab[CREATED].reason_phrase);
    clientsocket.getServResponse().setHeaders("Location", clientsocket.getHttpRequest().getHeaders().getRequest().Path);
    clientsocket.getServResponse().setHeaders("Server", "WebServ/1.0");
    clientsocket.getServResponse().setHeaders("Date", getCurrentDate());
}

void HttpRequestProcess::method_PUT(ClientSocket &clientsocket)
{
    (void)clientsocket;
    DEBUG_COUT("Process Method PUT");
    HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()), clientsocket.getServResponse(),
                                 clientsocket.getHttpRequest().getHeaders().getRequest().Path, NOT_IMPLEMENTED);
}

void HttpRequestProcess::method_DELETE(ClientSocket &clientsocket)
{
    DEBUG_COUT("Process Method DELETE");

    std::string path = clientsocket.getHttpRequest().getResolvedPath();

    if (fileExist(path) != 1)
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()), clientsocket.getServResponse(),
                                     clientsocket.getHttpRequest().getHeaders().getRequest().Path, INTERNAL_SERVER_ERROR);
        return;
    }

    std::remove(path.c_str());
    clientsocket.getServResponse().setStatusLine("HTTP/1.1 " + Syntax::status_codes_tab[NO_CONTENT].code_str + " " + Syntax::status_codes_tab[NO_CONTENT].reason_phrase);
    clientsocket.getServResponse().setHeaders("Server", "WebServ/1.0");
    clientsocket.getServResponse().setHeaders("Date", getCurrentDate());
}

void HttpRequestProcess::method_CONNECT(ClientSocket &clientsocket)
{
    (void)clientsocket;
    DEBUG_COUT("Process Method CONNECT");
    HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()), clientsocket.getServResponse(),
                                 clientsocket.getHttpRequest().getHeaders().getRequest().Path, NOT_IMPLEMENTED);
}

void HttpRequestProcess::method_OPTIONS(ClientSocket &clientsocket)
{
    (void)clientsocket;
    DEBUG_COUT("Process Method OPTION");
    HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()), clientsocket.getServResponse(),
                                 clientsocket.getHttpRequest().getHeaders().getRequest().Path, NOT_IMPLEMENTED);
}

void HttpRequestProcess::method_TRACE(ClientSocket &clientsocket)
{
    (void)clientsocket;
    DEBUG_COUT("Process Method TRACE");
    HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()), clientsocket.getServResponse(),
                                 clientsocket.getHttpRequest().getHeaders().getRequest().Path, NOT_IMPLEMENTED);
}

void HttpRequestProcess::method_PATCH(ClientSocket &clientsocket)
{
    (void)clientsocket;
    DEBUG_COUT("Process Method PATCH");
    HttpRequestProcess::setError(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ()), clientsocket.getServResponse(),
                                 clientsocket.getHttpRequest().getHeaders().getRequest().Path, NOT_IMPLEMENTED);
}