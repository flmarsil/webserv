/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Header_Structures.hpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/01 10:51:57 by fassani           #+#    #+#             */
/*   Updated: 2021/06/02 16:23:22 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_STRUCTURES_HPP
#define HEADER_STRUCTURES_HPP

#include <iostream>
#include <string.h>
#include <vector>



//https://developer.mozilla.org/fr/docs/Web/HTTP/Overview
typedef struct s_Request
{
    std::string Method;

    std::string Path;

    std::string Path_Info;

    std::string Query;

    std::string Version;

} t_Request;



//https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Accept-Charset
typedef struct s_Charset
{
    //Un jeu de caractères comme utf-8 ou iso-8859-15.
    std::string charset;

    // Toute valeur est placée dans un ordre de préférence exprimé
    // à l'aide d'une valeur de qualité relative appelée  weight.
    double q;

} t_Charset;

// https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Accept-Language
typedef struct s_Language
{
    // Une langue exprimée sous la forme de 2 ou 3 caractères.
    std::string langue;

    // Une balise de langue complète. En plus de la langue elle-même, elle peut contenir des informations additionnelles après un'-'.
    // L'information supplémentaire la plus courante est la variante de pays (telle que'en-US') ou le type d'alphabet à utiliser (comme'sr-Lat').
    // D'autres variantes comme le type d'orthographe ('de-DE-1996') ne sont pas habituellement utilisées dans le contexte de cet en-tête.
    std::string locale;

    // Une quantité numérique donnant un ordre de préférence et qui utilise une valeur de qualité relative, appelée poids.
    double q;

} t_Language;

// https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Allow
// https://developer.mozilla.org/fr/docs/Web/HTTP/Methods
typedef struct s_Allow
{
    // La méthode GET demande une représentation de la ressource spécifiée.
    // Les requêtes GET doivent uniquement être utilisées afin de récupérer des données.
    bool GET;

    // La méthode HEAD demande une réponse identique à une requête GET
    // pour laquelle on aura omis le corps de la réponse (on a uniquement l'en-tête).
    bool HEAD;

    // La méthode POST est utilisée pour envoyer une entité vers la ressource indiquée.
    // Cela  entraîne généralement un changement d'état ou des effets de bord sur le serveur.
    bool POST;

    // La méthode PUT remplace toutes les représentations actuelles de la ressource visée par le contenu de la requête.
    bool PUT;

    // La méthode DELETE supprime la ressource indiquée.
    bool DELETE;

    // La méthode CONNECT établit un tunnel vers le serveur identifié par la ressource cible.
    bool CONNECT;

    // La méthode OPTIONS est utilisée pour décrire les options de communications avec la ressource visée.
    bool OPTIONS;

    // La méthode TRACE réalise un message de test aller/retour en suivant le chemin de la ressource visée.
    bool TRACE;

    // La méthode PATCH est utilisée pour appliquer des modifications partielles à une ressource.
    bool PATCH;

} t_Allow;

// https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Authorization
typedef struct s_Authorization
{
    // Le type d'authentification. Le type "Basic" est souvent utilisé. Pour connaître les autres types :
    // https://www.iana.org/assignments/http-authschemes/http-authschemes.xhtml
    std::string type;

    // Si c'est le type d'authentification "Basic" qui est utilisé, les identifiants sont construits de la manière suivante :
    // L'identifiant de l'utilisateur et le mot de passe sont combinés avec deux-points : (aladdin:sesameOuvreToi).
    // Cette chaîne de caractères est ensuite encodée en base64 (YWxhZGRpbjpzZXNhbWVPdXZyZVRvaQ==).
    std::string credentials;

} t_Authorization;

// https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Content-Type
typedef struct s_Type
{
    // Le type MIME de la ressource ou des données.
    std::string media_type;

    // L'encodage utilisé pour les caractères des données.
    std::string charset;

    // Pour les entités fragmentées (multipart),la directive boundary est nécessaire.
    // Elle ne se termine pas par un espace et est composée de 1 à 70 caractères qui proviennent d'un ensemble
    // de caractères connus pour ne pas être transformés/modifiés par les différents composants au travers desquels transitent les emails.
    // Cette directive est utilisée afin d'encapsuler les limites des différents fragments d'un message fragmenté.
    std::string boundary;

} t_Type;

// https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Date
typedef struct s_Date
{
    // L'un des mots "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ou "Sun" (sensible à la casse).
    std::string day_name;

    // Numéro de jour à 2 chiffres, par ex. "04" ou "23"
    std::string day;

    // L'un des mots "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" (sensible à la casse)
    std::string month;

    // Numéro d'année à 4 chiffres, par exemple "1990" ou "2018".
    std::string year;

    // Numéro d'heure à 2 chiffres, par exemple "09" or "23".
    std::string hour;

    // Numéro d'heure à 2 chiffres, par exemple "04" or "59".
    std::string minute;

    // Numéro de seconde à 2 chiffres, par exemple "04" or "59".
    std::string second;

} t_Date;

// https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Host
typedef struct s_Host
{
    // le nom de domaine du serveur (pour de l'hébergement virtuel).
    std::string host;

    // Facultatif
    // numéro de port TCP sur lequel le serveur écoute.
    std::string port;

} t_Host;


// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Retry-After
typedef struct s_Retry_After
{
    // Une date après laquelle il faut réessayer. Voir l'en-tête Date pour plus de détails sur le format de date HTTP.
    t_Date http_date;

    // Un nombre entier décimal non négatif indiquant les secondes à attendre après la réception de la réponse.
    size_t delay_seconds;

} t_Retry_After;


// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent
typedef struct s_User_Agent
{
    // Un identifiant de produit - son nom ou son nom de code de développement.
    std::string product;

    // Numéro de version du produit.
    std::string product_version;

    // Zéro ou plusieurs commentaires contenant plus de détails ; des informations sur les sous-produits, par exemple.
    std::string comment;


} t_User_Agent;


// https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/WWW-Authenticate
typedef struct s_WWW_Authenticate
{
    // Une description de la zone protégée. Si aucun domaine n'est spécifié,
    // les clients affichent souvent un nom de domaine formaté à la place.
    std::string type;

    // Une description de la zone protégée.
    // Si aucun domaine n'est spécifié, les clients affichent souvent un nom de domaine formaté à la place.
    std::string realm;

    // Indique au client le schéma d'encodage préféré du serveur lorsqu'on soumet un nom d'utilisateur et un mot de passe.
    // La seule valeur acceptée est la chaine insensible à la casse "UTF-8".
    // Cela ne s'applique pas à l'encodage de la chaine du domaine.
    std::string charset;

} t_WWW_Authenticate;

#endif