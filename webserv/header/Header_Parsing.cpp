/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Header_Parsing.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/02 20:01:55 by fassani           #+#    #+#             */
/*   Updated: 2021/06/08 18:17:44 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Header_Parsing.hpp"
#include <sstream>

t_Date pars_date(std::string &str)
{
    t_Date date;
    if (str.empty())
        return (date);

    if (str.at(0) == ' ')
        str.erase(0, 1);
    int position = 0;
    int lpost = 0;

    position = str.find(',', 0);
    date.day_name = str.substr(0, position);
    position += 2;

    lpost = position;
    position = str.find(' ', lpost);
    date.day = str.substr(lpost, (position - lpost));
    position++;
    lpost = position;
    position = str.find(' ', lpost);
    date.month = str.substr(lpost, (position - lpost));
    position++;

    lpost = position;
    position = str.find(' ', lpost);
    date.year = str.substr(lpost, (position - lpost));
    position++;

    lpost = position;
    position = str.find(':', lpost);
    date.hour = str.substr(lpost, (position - lpost));
    position++;

    lpost = position;
    position = str.find(':', lpost);
    date.minute = str.substr(lpost, (position - lpost));
    position++;

    lpost = position;
    position = str.find(' ', lpost);
    date.second = str.substr(lpost, (position - lpost));

    return (date);
}

t_Request parsing_header_request(std::string &str_request)
{
    t_Request requestObj;

    int position, lpost = 0;
    position = str_request.find(' ', 0);
    requestObj.Method = str_request.substr(0, position);
    position++;
    lpost = position;

    if (str_request.find('?', lpost) == std::string::npos)
    {
        position = str_request.find(' ', lpost);
        requestObj.Path = str_request.substr(lpost, (position - lpost));
        position++;
    }
    else
    {
        position = str_request.find('?', lpost);
        requestObj.Path = str_request.substr(lpost, (position - lpost));
        position++;
        lpost = position;

        position = str_request.find(' ', lpost);
        requestObj.Query = str_request.substr(lpost, (position - lpost));
        position++;
    }

    requestObj.Version = str_request.substr(position);

    return (requestObj);
}

// exemple: Accept-Charset: utf-8, iso-8859-1;q=0.5, *;q=0.1
std::vector<t_Charset> parsing_Accept_Charset(std::string &str)
{
    std::vector<t_Charset> liste_charset;
    if (str.empty())
        return (liste_charset);
    std::vector<std::string> tmp_list = split_string(str, ",");

    for (size_t i = 0; i < tmp_list.size(); i++)
    {
        if (!tmp_list.at(i).empty())
        {
            int position = 0;
            t_Charset charset;
            if (tmp_list.at(i).find(';', position) == std::string::npos)
            {
                charset.charset = tmp_list.at(i);
                if (charset.charset.at(0) == ' ')
                    charset.charset.erase(0, 1);
                charset.q = 0;
            }
            else
            {
                position = tmp_list.at(i).find(';', 0);
                charset.charset = tmp_list.at(i).substr(0, position);
                if (charset.charset.at(0) == ' ')
                    charset.charset.erase(0, 1);
                position = tmp_list.at(i).find('=', 0);
                position++;
                charset.q = atof(tmp_list.at(i).substr(position).c_str());
            }
            liste_charset.push_back(charset);
        }
    }

    return (liste_charset);
}

// exemple: Accept-Language: fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5
std::vector<t_Language> parsing_Accept_Language(std::string &str)
{
    std::vector<t_Language> liste_language;
    if (str.empty())
        return (liste_language);
    std::vector<std::string> tmp_list = split_string(str, ",");
    for (size_t i = 0; i < tmp_list.size(); i++)
    {
        if (!tmp_list.at(i).empty())
        {
            int position = 0;
            t_Language language;
            if (tmp_list.at(i).find(';', position) == std::string::npos)
            {
                if (tmp_list.at(i).find('-', position) == std::string::npos)
                {
                    language.langue = tmp_list.at(i);
                    if (language.langue.at(0) == ' ')
                        language.langue.erase(0, 1);
                    language.q = 0;
                }
                else
                {
                    language.locale = tmp_list.at(i);
                    if (language.locale.at(0) == ' ')
                        language.locale.erase(0, 1);
                    language.q = 0;
                }
            }
            else
            {
                position = tmp_list.at(i).find(';', 0);
                std::string buffer = tmp_list.at(i).substr(0, position);
                if (tmp_list.at(i).find('-', position) == std::string::npos)
                {
                    language.langue = buffer;
                    if (language.langue.at(0) == ' ')
                        language.langue.erase(0, 1);
                }
                else
                {
                    language.locale = buffer;
                    if (language.locale.at(0) == ' ')
                        language.locale.erase(0, 1);
                }

                position = tmp_list.at(i).find('=', 0);
                position++;
                language.q = atof(tmp_list.at(i).substr(position).c_str());
            }
            liste_language.push_back(language);
        }
    }

    return (liste_language);
}

// exemple: Allow: GET, POST, HEAD
t_Allow parsing_Allow(std::string &str)
{
    t_Allow allow;
    if (str.empty())
        return (allow);

    allow.GET = str.find("GET", 0) != std::string::npos ? true : false;
    allow.HEAD = str.find("HEAD", 0) != std::string::npos ? true : false;
    allow.POST = str.find("POST", 0) != std::string::npos ? true : false;
    allow.PUT = str.find("PUT", 0) != std::string::npos ? true : false;
    allow.DELETE = str.find("DELETE", 0) != std::string::npos ? true : false;
    allow.CONNECT = str.find("CONNECT", 0) != std::string::npos ? true : false;
    allow.OPTIONS = str.find("OPTIONS", 0) != std::string::npos ? true : false;
    allow.TRACE = str.find("TRACE", 0) != std::string::npos ? true : false;
    allow.PATCH = str.find("PATCH", 0) != std::string::npos ? true : false;
    return (allow);
}

// exemple: Authorization: Basic YWxhZGRpbjpvcGVuc2VzYW1l
t_Authorization parsing_Authorization(std::string &str)
{
    t_Authorization autorization;
    if (str.empty())
        return (autorization);
    int position = 0;

    if (str.at(0) == ' ')
        str.erase(0, 1);

    position = str.find(' ', 0);
    autorization.type = str.substr(0, position);
    position++;
    autorization.credentials = str.substr(position);
    return (autorization);
}

// exemple: Content-Language: de, en
std::vector<std::string> parsing_Content_Language(std::string &str)
{
    std::vector<std::string> tmp_list;
    if (str.empty())
        return (tmp_list);

    tmp_list = split_string(str, ",");

    for (size_t i = 0; i < tmp_list.size(); i++)
    {
        if (!tmp_list.at(i).empty())
        {
            if (tmp_list.at(i).at(0) == ' ')
                tmp_list.at(i).erase(0, 1);
        }
    }
    return (tmp_list);
}

// exemple:  Content-Length: 2088
size_t parsing_Content_Length(std::string &str)
{
    size_t content_length = 0;
    if (str.empty())
        return (content_length);

    if (str.at(0) == ' ')
        str.erase(0, 1);

    std::stringstream stream(str);
    stream >> content_length;
    return (content_length);
}

// exemple: Content-Location: /my-first-blog-post
std::string parsing_Content_Location(std::string &str)
{
    if (str.empty())
        return ("");
    if (str.at(0) == ' ')
        str.erase(0, 1);
    return (str);
}

// exemple: Content-Type: multipart/form-data; boundary=---------------------------974767299852498929531610575
t_Type parsing_Content_Type(std::string &str)
{
    t_Type content_type;
    if (str.empty())
        return (content_type);

    if (str.at(0) == ' ')
        str.erase(0, 1);

    int position = 0;
    int lpost = 0;

    if (str.find(';', position) == std::string::npos)
        content_type.media_type = str;
    else
    {
        position = str.find(';', 0);
        content_type.media_type = str.substr(0, position);

        if (str.find("boundary=", 0) != std::string::npos)
        {
            position = str.find("boundary=", 0);
            position += 9;
            if (str.find(";", position) == std::string::npos)
                content_type.boundary = str.substr(position);
            else
            {
                lpost = position;
                position = str.find(";", lpost);
                content_type.boundary = str.substr(lpost, (position - lpost));
            }
        }

        if (str.find("charset=", 0) != std::string::npos)
        {
            position = str.find("charset=", 0);
            position += 8;
            if (str.find(";", position) == std::string::npos)
                content_type.charset = str.substr(position);
            else
            {
                lpost = position;
                position = str.find(";", lpost);
                content_type.charset = str.substr(lpost, (position - lpost));
            }
        }
    }

    return (content_type);
}

// exemple: Date: Wed, 21 Oct 2015 07:28:00 GMT
t_Date parsing_Date(std::string &str)
{
    return (pars_date(str));
}

// exemple: Host: developer.mozilla.org
t_Host parsing_Host(std::string &str)
{
    t_Host host;
    if (str.empty())
        return (host);
    int position = 0;

    if (str.find(':', position) == std::string::npos)
    {
        position = str.find(':', 0);
        host.host = str.substr(0, position);
        if (host.host.at(0) == ' ')
            host.host.erase(0, 1);
        host.port = "80";
    }
    else
    {
        position = str.find(':', 0);
        host.host = str.substr(0, position);
        if (host.host.at(0) == ' ')
            host.host.erase(0, 1);
        position++;
        host.port = str.substr(position);
    }

    return (host);
}

// exemple: Last-Modified: Wed, 21 Oct 2015 07:28:00 GMT
t_Date parsing_Last_Modified(std::string &str)
{
    return (pars_date(str));
}

// exemple: Location: /index.html
std::string parsing_Location(std::string &str)
{
    if (str.empty())
        return ("");
    if (str.at(0) == ' ')
        str.erase(0, 1);
    return (str);
}

// exemple: Referer: https://developer.mozilla.org/fr/docs/Web/JavaScript
std::string parsing_Referer(std::string &str)
{
    if (str.empty())
        return ("");
    if (str.at(0) == ' ')
        str.erase(0, 1);
    return (str);
}

// exemple: Retry-After: Wed, 21 Oct 2015 07:28:00 GMT
// exemple: Retry-After: 120
t_Retry_After parsing_Retry_After(std::string &str)
{
    t_Retry_After retry_after;
    if (str.empty())
        return (retry_after);

    return (retry_after);
}

// exemple: Server: Apache/2.4.1 (Unix)
std::string parsing_Server(std::string &str)
{
    if (str.empty())
        return ("");
    if (str.at(0) == ' ')
        str.erase(0, 1);
    return (str);
}

// exemple: Transfer-Encoding: gzip, chunked
std::vector<std::string> parsing_Transfer_Encoding(std::string &str)
{
    std::vector<std::string> tmp_list;
    if (str.empty())
        return (tmp_list);

    tmp_list = split_string(str, ",");

    for (size_t i = 0; i < tmp_list.size(); i++)
    {
        if (!tmp_list.at(i).empty())
        {
            if (tmp_list.at(i).at(0) == ' ')
                tmp_list.at(i).erase(0, 1);
        }
    }
    return (tmp_list);
}

// exemple: User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36
std::string parsing_User_Agent(std::string &str)
{
    if (str.empty())
        return ("");
    if (str.at(0) == ' ')
        str.erase(0, 1);
    return (str);
}

// exemple: WWW-Authenticate: Basic realm="Accès au site de staging", charset="UTF-8"
t_WWW_Authenticate parsing_WWW_Authenticate(std::string &str)
{
    t_WWW_Authenticate www_authenticate;
    if (str.empty())
        return (www_authenticate);
    if (str.at(0) == ' ')
        str.erase(0, 1);

    int position = 0;
    int lpost = 0;

    position = str.find(' ', 0);
    www_authenticate.type = str.substr(0, position);

    if (str.find("realm=", 0) != std::string::npos)
    {
        position = str.find("realm=", 0);
        position += 5;
        if (str.find(",", position) == std::string::npos)
            www_authenticate.realm = str.substr(position);
        else
        {
            lpost = position;
            position = str.find(",", lpost);
            www_authenticate.realm = str.substr(lpost, (position - lpost));
        }
    }

    if (str.find("charset=", 0) != std::string::npos)
    {
        position = str.find("charset=", 0);
        position += 8;
        if (str.find(",", position) == std::string::npos)
            www_authenticate.charset = str.substr(position);
        else
        {
            lpost = position;
            position = str.find(",", lpost);
            www_authenticate.charset = str.substr(lpost, (position - lpost));
        }
    }

    return (www_authenticate);
}