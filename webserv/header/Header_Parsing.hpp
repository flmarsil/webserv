/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Header_Parsing.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/02 20:01:59 by fassani           #+#    #+#             */
/*   Updated: 2021/05/21 10:17:41 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_PARSING_HPP
#define HEADER_PARSING_HPP

#include "Header_Structures.hpp"
#include "Header_Request.hpp"
#include "../utils/utils.hpp"
#include <stdlib.h>
#include <string>
#include <map>


// typedef int (*funcParsing)(Header_request&, std::string);
// std::map<std::string, funcParsing> map_header_funcParsing;

t_Request parsing_header_request(std::string &str_request);
std::vector<t_Charset> parsing_Accept_Charset(std::string &str);
std::vector<t_Language> parsing_Accept_Language(std::string &str);
t_Allow parsing_Allow(std::string &str);
t_Authorization parsing_Authorization(std::string &str);
std::vector<std::string> parsing_Content_Language(std::string &str);
size_t parsing_Content_Length(std::string &str);
std::string parsing_Content_Location(std::string &str);

t_Type parsing_Content_Type(std::string &str);
t_Date parsing_Date(std::string &str);
t_Host parsing_Host(std::string &str);
t_Date parsing_Last_Modified(std::string &str);
std::string parsing_Location(std::string &str);
std::string parsing_Referer(std::string &str);
t_Retry_After parsing_Retry_After(std::string &str);
std::string parsing_Server(std::string &str);
std::vector<std::string> parsing_Transfer_Encoding(std::string &str);
std::string parsing_User_Agent(std::string &str);
t_WWW_Authenticate parsing_WWW_Authenticate(std::string &str);

#endif