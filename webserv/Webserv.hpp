#ifndef WEBSERV_HPP
#define WEBSERV_HPP

# define RED "\033[1;31m"
# define GREEN "\033[1;32m"
# define BLUE "\033[1;34m"
# define CYAN "\033[1;36m"
# define YELLOW "\033[1;33m"
# define RESET "\033[0m"

#define WHITESPACES  " \t\n\r\f\v"

#define MAX_URI_LEN 8000
#define MAX_HEADER_LEN 8000
#define BUFFER_SIZE_REQUEST 5000

#define FAILURE 1
#define SUCCESS 0

// libraries
#include <iostream>
#include <fstream>
#include <string>
#include <exception>
#include <algorithm>
#include <ctime>
#include <sys/time.h>
#include <sys/stat.h>
#include <csignal>
#include <errno.h>

// networking
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

// containers
#include <vector>
#include <list>
#include <map>
#include <set>

// includes
#include "utils/Syntax.hpp"
#include "header/Header_Request.hpp"
#include "header/Header_Structures.hpp"
#include "header/Header_Parsing.hpp"
#include "utils/utils.hpp"
#include "parsing/ConfigServer.hpp"
#include "socket/ServerSocket.hpp"
#include "socket/ClientSocket.hpp"
#include "server/HttpRequest.hpp"
#include "server/HttpResponse.hpp"
#include "server/HttpServer.hpp"
#include "server/HttpRequestParser.hpp"
#include "server/HttpRequestProcess.hpp"
#include "parsing/ConfigFileParser.hpp"
#include "utils/Debugguer.hpp"
#include "env/Env.hpp"
#include "cgi/Cgi.hpp"


#endif
