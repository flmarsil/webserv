#include "../Webserv.hpp"

/* Constructor */
ServerSocket::ServerSocket(int backlog)
    : _socket(0), _backlog(backlog) {}

/* Destructor */
ServerSocket::~ServerSocket() {}

/* Copy constructor */
ServerSocket::ServerSocket(const ServerSocket& copy)
    : _port(copy._port), _servAddr(copy._servAddr), _socket(copy._socket), _backlog(copy._backlog) {}

/* Overloading operator */
const ServerSocket& ServerSocket::operator = (const ServerSocket& assignObj) {
    _port = assignObj.getPort();
    _servAddr = assignObj.getServAddr();
    _socket = assignObj.getSocket();
    _backlog = assignObj.getBacklog();
    return (*this);
}

/* Methods */
void ServerSocket::createListeningSocket() {
    _createSocket();
    _changeSocketOptions();
    _bindSocket();
    _setListeningMode();
    DEBUG_COUT("Socket " << _socket << " a bien été mis en écoute sur le port " << _port);
}

/* Private methods */
void ServerSocket::_createSocket() {
    if ((_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cerr << RED"Erreur lors de la création du socket" << RESET << std::endl;
        // std::cerr << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }
    if (fcntl(_socket, F_SETFL, O_NONBLOCK) < 0) {
		// DEBUG_COUT("Fcntl erreur avec F_SETFL : " << strerror(errno));
		exit(EXIT_FAILURE);
	}
    DEBUG_COUT("Le socket " << _socket << " a bien été crée");
}

void ServerSocket::_changeSocketOptions() {
	if (setsockopt(_socket, SOL_SOCKET, SO_REUSEADDR,
		&_reuseAddr, sizeof(_reuseAddr)) < 0) {
		std::cerr << "Erreur lors du changement des options du socket : ";
		// std::cerr << strerror(errno) << std::endl;
		close(_socket);
		exit(EXIT_FAILURE);
	}
    DEBUG_COUT("Les options du socket " << _socket << " ont bien été changées");
}

void ServerSocket::_bindSocket() {
    _port = _configServ->getPort();
    memset((char*)& _servAddr, 0, sizeof(_servAddr));
    _servAddr.sin_family = AF_INET;
    _servAddr.sin_addr.s_addr = INADDR_ANY;
    _servAddr.sin_port = htons(_port);
    if (bind(_socket, (struct sockaddr*)& _servAddr, sizeof(_servAddr)) < 0) {
        std::cerr << RED"Erreur lors du binding sur le port " << _port << RESET << std::endl;
        // std::cerr << strerror(errno) << std::endl;
        close(_socket);
        exit(EXIT_FAILURE);
    }
    DEBUG_COUT("Le socket " << _socket << " a bien été lié au port " << _port);
}

void ServerSocket::_setListeningMode() {
    if (listen(_socket, _backlog) < 0) {
        std::cerr << RED"Erreur lors l'écoute sur le socket " << _socket << RESET << std::endl;
        // std::cerr << strerror(errno) << std::endl;
        close(_socket);
        exit(EXIT_FAILURE);
    }
    DEBUG_COUT("Paramètrage du socket " << _socket << " comme socket d'écoute sur le port " << _port);
}

/* Setters */
void ServerSocket::setConfigServ(ConfigServer* configServ) {
    _configServ = configServ;
}
/* Getters */
ServerSocket::PORT ServerSocket::getPort() const {
    return _port; 
}

ServerSocket::SOCKET ServerSocket::getSocket() const {
    return _socket;
}

ServerSocket::SOCK_ADDR ServerSocket::getServAddr() const {
    return _servAddr; 
}

int ServerSocket::getBacklog() const { 
    return _backlog;
}

const ConfigServer* ServerSocket::getConfigServ() const {
    return _configServ;
}