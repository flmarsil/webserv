#ifndef SERVERSOCKET_HPP
#define SERVERSOCKET_HPP

#include "../Webserv.hpp"

/*
    ServerSocket permet de créer un socket et de le mettre en écoute 
    sur un port définit.
*/

#define DEFAULT_BACKLOG 1000

class ConfigServer;

class ServerSocket
{
public:
    /* Aliases definition  */
    typedef int                 SOCKET;
    typedef struct sockaddr_in  SOCK_ADDR;
    typedef int                 PORT;

public:
    /* Constructor */
    ServerSocket(int backlog = DEFAULT_BACKLOG);
    /* Destructor */
    ~ServerSocket();
    /* Copy constructor */
    ServerSocket(const ServerSocket& copy);
    /* Overloading operator */
    const ServerSocket& operator = (const ServerSocket& assignObj);
    /* Methods */
    void createListeningSocket();
    // Setters
    void setConfigServ(ConfigServer* configServ);

    // Getters
    const ConfigServer* getConfigServ() const;
    PORT getPort() const;
    SOCKET getSocket() const;
    SOCK_ADDR getServAddr() const;
    int getBacklog() const;

private:
    /* Attributs */
    ConfigServer*   _configServ;

    PORT            _port;
    SOCK_ADDR       _servAddr;
    SOCKET          _socket;
    int             _reuseAddr;
    int             _backlog;

    /* Private methods */
    void _createSocket();
    void _changeSocketOptions();
    void _bindSocket();
    void _setListeningMode();

};  // class ServerSocket

#endif
