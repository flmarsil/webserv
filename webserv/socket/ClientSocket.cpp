#include "ClientSocket.hpp"

/* Constructor */
ClientSocket::ClientSocket(SOCKET newSocket, SOCK_ADDR newCliAddr, const std::list<const ConfigServer*>& configServer, int port) 
    :   _newSocket(newSocket),
        _servPort(port),
        _cliAddr(newCliAddr),
        _cliRequest(),
        _servResponse(),
        _connectionRefused(false),
        _closing(false),
        _configServ(configServer){}


/* Destructor */
ClientSocket::~ClientSocket() {}

/* Copy constructor */
ClientSocket::ClientSocket(const ClientSocket& copy) 
    :   _newSocket(copy._newSocket),
        _servPort(copy._servPort),
        _cliAddr(copy._cliAddr),
        _cliRequest(copy._cliRequest),
        _connectionRefused(copy._connectionRefused),
        _closing(copy._closing),
        _configServ(copy._configServ) {}

/* Overloading operator */
const ClientSocket& ClientSocket::operator = (const ClientSocket& assignObj) {
    _newSocket = assignObj.getSocket();
    _servPort = assignObj.getPort();
    _cliAddr = assignObj.getCliAddr();
    _cliRequest = assignObj.getHttpRequest();
    _connectionRefused = assignObj.getConnectionAuthorisation();
    _closing = assignObj.getConnnectionStatus();
    
    return (*this);
}


/* Methods */
void ClientSocket::receiveHttpRequest(const char* buffer, int lenRecptMsg) {
    if (_cliRequest.getStatusRequest() == HttpRequest::NONE)
        _cliRequest.setStatusRequest(HttpRequest::START);
    
    for (int i = 0 ; i < lenRecptMsg ; i++)
        _cliRequest.getRawRequest().push_back(static_cast<byte>(buffer[i]));

    HttpRequestParser::parsing(this->_cliRequest, HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), lenRecptMsg);    // lancement du parsing de la requete Http;
}

int ClientSocket::sendHttpResponse() {
    if (_cliRequest.getStatusRequest() != HttpRequest::REQUEST_PROCESSED)
        return (SUCCESS);

    int ret;
    char* HTTP_Response = _servResponse.getResponse();
    _servResponse.deleteBody();
    //std::cout << HTTP_Response << std::endl;

    if ((ret = send(_newSocket, HTTP_Response, _servResponse.getLenghtResponse(), 0)) < 0) {
        DEBUG_COUT("Erreur durant l'écriture sur le socket : (" << _newSocket << ")");
        if (HTTP_Response)
            free(HTTP_Response);
        std::remove(this->getHttpRequest().getHeaders().getBody_tmp_path().c_str());
        std::remove(this->getServResponse().getCgiResponsePath().c_str());
        return (FAILURE);
    }
    else {
        DEBUG_COUT("Réponse Http envoyée au socket " << _newSocket);
        // DEBUG_COUT("Connexion avec le socket " << _newSocket << " terminée");
        if (HTTP_Response)
            free(HTTP_Response);
        std::remove(this->getHttpRequest().getHeaders().getBody_tmp_path().c_str());
        std::remove(this->getServResponse().getCgiResponsePath().c_str());
        return (FAILURE);
    }
    
    return (SUCCESS);
}

void (*method_func[9])(ClientSocket &clientsocket) = {
            HttpRequestProcess::method_GET,
            HttpRequestProcess::method_HEAD,
            HttpRequestProcess::method_POST,
            HttpRequestProcess::method_PUT,
            HttpRequestProcess::method_DELETE,
            HttpRequestProcess::method_CONNECT,
            HttpRequestProcess::method_OPTIONS,
            HttpRequestProcess::method_TRACE,
            HttpRequestProcess::method_PATCH};

int ClientSocket::process()
{
    //d'abord vérifier si c'est une requete à exécuter avec un cgi
    //si oui exécuter avec cgi sinon exécuter normalement

    // std::cout << "Processing request ..." << std::endl;

    if ((this->getHttpRequest().getHeaders().getRequest().Path.size() +
    this->getHttpRequest().getHeaders().getRequest().Query.size()) > MAX_URI_LEN)
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()) , this->getServResponse(),
        this->getHttpRequest().getHeaders().getRequest().Path, URI_TOO_LONG);
        this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
        return (0);
    }

    if(this->getHttpRequest().getHeaders().getHeader_length() > MAX_HEADER_LEN)
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()) , this->getServResponse(),
        this->getHttpRequest().getHeaders().getRequest().Path, PAYLOAD_TOO_LARGE);
        this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
        return (0);
    }

    if ((this->getHttpRequest().getHeaders().getRequest().Version != "HTTP/1.0" &&
        this->getHttpRequest().getHeaders().getRequest().Version != "HTTP/1.1"))
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()) , this->getServResponse(),
        this->getHttpRequest().getHeaders().getRequest().Path, HTTP_VERSION_NOT_SUPPORTED);
        this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
        return (0);
    }
    

    //check if method allowed
    if (!HttpRequestProcess::methodChecker(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), this->getHttpRequest()))
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), this->getServResponse(),
                                     this->getHttpRequest().getHeaders().getRequest().Path, METHOD_NOT_ALLOWED);

        std::list<std::string> methodsList = HttpRequestProcess::getLocationMethods(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), this->getHttpRequest().getHeaders().getRequest().Path);

        std::list<std::string>::const_iterator it = methodsList.begin();
        std::list<std::string>::const_iterator ite = methodsList.end();
        std::string allow;
        while (it != ite)
        {
            allow.append(*it + ", ");
            it++;
        }
        allow.resize(allow.size() - 2);
        this->_servResponse.setHeaders("Allow", allow);
        this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
        return (0);
    }

    //check client_max_body_size
    if (HttpRequestProcess::isSizeOverload(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), this->getHttpRequest()))
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()) , this->getServResponse(),
        this->getHttpRequest().getHeaders().getRequest().Path, PAYLOAD_TOO_LARGE);
        this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
        return (0);
    }
                   
    std::string resolvedPath = HttpRequestProcess::resolvePath(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), this->getHttpRequest(),this->getHttpRequest().getHeaders().getRequest().Path);
    // std::cout << GREEN << "Resolved path :" + resolvedPath << RESET << std::endl;
    this->getHttpRequest().setResolvedPath(resolvedPath);
    int retCode = fileExist(resolvedPath);
    if (retCode == 0 && this->getHttpRequest().getHeaders().getRequest().Method == "GET")//file not found
    {
        HttpRequestProcess::setError(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), this->getServResponse(),
                                     this->getHttpRequest().getHeaders().getRequest().Path, NOT_FOUND);
        this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
        return (0);
    }
    else if (retCode == 2 && this->getHttpRequest().getHeaders().getRequest().Method == "GET") //dossier
    {
        if (HttpRequestProcess::isAutoIndex(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), this->getHttpRequest()))
        {
            if (resolvedPath.at(resolvedPath.size() - 1) == '/')
            {
                this->_servResponse.setStatusLine("HTTP/1.1 " + Syntax::status_codes_tab[OK].code_str + " " + Syntax::status_codes_tab[OK].reason_phrase);
            }else
            {
                this->_servResponse.setStatusLine("HTTP/1.1 " + Syntax::status_codes_tab[MOVED_PERMANENTLY].code_str + " " + Syntax::status_codes_tab[MOVED_PERMANENTLY].reason_phrase);
                this->_servResponse.setHeaders("Location", this->getHttpRequest().getHeaders().getRequest().Path + "/");
            }
            
            
            size_t size = 0;
            _servResponse.setBody(NULL);
            _servResponse.setBody(HttpRequestProcess::getAutoIndex(resolvedPath , this->getHttpRequest().getHeaders().getRequest().Path ,&size));
            _servResponse.setSizeOfBody(size);
            this->_servResponse.setHeaders("Content-Type", "text/html; charset=UTF-8");
            this->_servResponse.setHeaders("Server", "WebServ/1.0");
            this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
            return (0);
        }
        else
        {
            //send 404 error
            HttpRequestProcess::setError(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), this->getServResponse(),
                                         this->getHttpRequest().getHeaders().getRequest().Path, NOT_FOUND);
            this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
            return (0);
        }
    }

    std::string cgiPath = HttpRequestProcess::isExecutedByCgi(HttpRequestProcess::getConf(this->getPort(), this->getConfigServ()), this->getHttpRequest(), resolvedPath);
    if (!cgiPath.empty())
    {
        //doit etre executé par un cgi
        this->_servResponse.setCgiResponsePath(Cgi::execute(cgiPath, *this));
        this->_servResponse.setStatusLine("HTTP/1.1 " + Syntax::status_codes_tab[OK].code_str + " " +Syntax::status_codes_tab[OK].reason_phrase);
        this->_servResponse.setHeaders("Date", getCurrentDate());
        this->_servResponse.setIsCgiBody();
        this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
    }
    else
    {
        // pas de cgi

        method_func[std::distance(HttpRequestProcess::method_list, std::find(HttpRequestProcess::method_list,
        HttpRequestProcess::method_list + 9,
        this->getHttpRequest().getHeaders().getRequest().Method))](*this);
         this->getHttpRequest().setStatusRequest(HttpRequest::REQUEST_PROCESSED);
    }

    if (fileExist(this->getHttpRequest().getHeaders().getBody_tmp_path()) == 1) { //delete temp body {
        std::remove(this->getHttpRequest().getHeaders().getBody_tmp_path().c_str());
    }
    
    return (0);
}

int ClientSocket::processConnectionRefused() {
    /*
        Mettre dans la réponse http version + status code
        _closing = true;
        processError();
    */
    return (FAILURE);
}

// Getters
ClientSocket::PORT ClientSocket::getPort() const {
    return _servPort;
}

ClientSocket::SOCKET ClientSocket::getSocket() const {
    return _newSocket;
}

ClientSocket::SOCK_ADDR ClientSocket::getCliAddr() const {
    return _cliAddr;
}

const HttpRequest& ClientSocket::getHttpRequest() const {
    return _cliRequest;
}

HttpRequest& ClientSocket::getHttpRequest() {
    return _cliRequest;
}

HttpResponse& ClientSocket::getServResponse() {
    return _servResponse;
}

bool ClientSocket::getConnectionAuthorisation() const {
    return _connectionRefused;
}

bool ClientSocket::getConnnectionStatus() const {
    return _closing;
}

const std::list<const ConfigServer*> &ClientSocket::getConfigServ() const {
    return _configServ;
}

// Setters
void ClientSocket::setClosing(bool status) {
    _closing = status;
}
