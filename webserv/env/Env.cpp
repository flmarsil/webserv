/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Env.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/11 23:42:51 by fassani           #+#    #+#             */
/*   Updated: 2021/06/04 09:16:11 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Env.hpp"

Env::Env()
{
}

Env::~Env()
{
    clear();
}

Env::Env(const Env &other)
{
    *this = other;
}

Env::Env(char ***other)
{
    this->setEnv(other);
}

const Env &Env::operator=(const Env &other)
{
    this->env = other.env;

    return (*this);
}

void Env::setEnv(char ***other)
{

    if (!(*other))
        return;
    this->env.clear();
    int i = 0;
    while ((*other)[i])
    {
        int position = 0;
        std::string str((*other)[i]);
        position = str.find('=', 0);
        std::string key = str.substr(0, position);
        position++;
        std::string value = str.substr(position);
        this->add(key, value);
        i++;
    }
}

int Env::append(char ***other)
{
    if (!(*other))
        return (0);
    int i = 0;
    while (other[i])
    {
        int position = 0;
        std::string str((*other)[i]);
        position = str.find('=', 0);
        std::string key = str.substr(0, position);
        position++;
        std::string value = str.substr(position);
        this->add(key, value);
        i++;
    }
    return (i);
}

std::ostream &operator<<(std::ostream &out, Env &envi)
{
    std::string str;
    for (std::map<std::string, std::string>::iterator it = envi.getMap().begin(); it != envi.getMap().end(); ++it)
        str.append(it->first + "=" + it->second + "\n");
    return (out << str);
}

char **Env::getEnv()
{
    char **tmp_env = NULL;
    if (!(tmp_env = static_cast<char **>(malloc(sizeof(char *) * (this->env.size() + 1)))))
        return (0);
    int i = 0;
    for (std::map<std::string, std::string>::iterator it = this->env.begin(); it != this->env.end(); ++it)
    {
        std::string str = it->first + "=" + it->second;
        char *buffer = NULL;
        if (!(buffer = static_cast<char *>(malloc(sizeof(char) * (str.size() + 1)))))
            return (0);

        for (size_t y = 0; y < str.size(); y++)
            buffer[y] = str.at(y);

        buffer[str.size()] = 0;
        tmp_env[i] = buffer;
        i++;
    }
    tmp_env[i] = 0;
    this->ptrEnv.push_back(tmp_env);
    return (tmp_env);
}

std::map<std::string, std::string> &Env::getMap()
{
    return (this->env);
}

void Env::add(std::string key, std::string value)
{
    if (key.empty() || value.empty())
        return ;
    this->env[key] = value;
}

void Env::erase(std::string key)
{
    this->env.erase(key);
}

std::string Env::get(std::string key)
{
    return (this->env[key]);
}

void Env::clear()
{
    this->env.clear();
     for (size_t i = 0; i < this->ptrEnv.size(); i++)
    {
        std::cout << "delete env pointeur " << i << std::endl;
        free_env(&this->ptrEnv.at(i));
    }
    ptrEnv.clear();
}

size_t Env::size()
{
    return (this->env.size());
}

void Env::free_env(char ***array)
{
    // if(*array){
    //   delete[] *array;  
    // }
    
    int i;

    if (!(*array))
        return;
    i = 0;
    while ((*array)[i])
    {
        if ((*array)[i])
        {
            free((*array)[i]);
            (*array)[i] = 0;
        }
        i++;
    }
    if ((*array)[i])
    {
        free((*array)[i]);
        (*array)[i] = 0;
    }
    free((*array));
    *array = 0;
}
