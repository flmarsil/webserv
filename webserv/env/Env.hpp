/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Env.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/11 23:42:56 by fassani           #+#    #+#             */
/*   Updated: 2021/05/14 18:16:46 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENV_HPP
#define ENV_HPP

#include <iostream>
#include <iterator>
#include <map>
#include <vector>
#include <stdlib.h>

class Env
{
    private:
    std::map<std::string, std::string> env;
    std::vector <char **>ptrEnv;
    public:
    Env();
    ~Env();

    Env(const Env &other);
    Env(char ***other);
    const Env &operator=(const Env &other);

    void setEnv(char ***other);
    int append(char ***other);

    char **getEnv();
    std::map<std::string, std::string> &getMap();

    void add(std::string key, std::string value);
    void erase(std::string key);
    std::string get(std::string key);
    void clear();
    size_t size();
    static void	free_env(char ***array);

};

std::ostream &operator<<(std::ostream &out, Env &envi);

#endif