#include "Webserv.hpp"

int     check_arguments(int ac, char** av, std::string& filepath)
{
    if (ac > 3)
        return (FAILURE);
    for (int i = 1 ; i < ac ; i++)
    {
        std::string argument = av[i];
        if (argument == "-v")
            DEBUG_START(true);
        else
            filepath = argument;
    }
    filepath = (filepath.empty()) ? "./webserv/config/default.conf" : filepath;
    return (SUCCESS);
}

int     main(int ac, char** av)
{
    HttpServer WebServer;
    std::string filepath;
    // http://www.linux-france.org/article/man-fr/man2/signal-2.html
    // https://fr.wikipedia.org/wiki/SIGINT_(POSIX)
    if (signal(SIGINT, &HttpServer::setSignal) == SIG_ERR)
		return (EXIT_FAILURE);
    if (check_arguments(ac, av, filepath) == FAILURE)
        return (EXIT_FAILURE);
    if (WebServer.parsing(filepath) == FAILURE)
        return (EXIT_FAILURE);
    if (WebServer.setupServers() == FAILURE)
        return (EXIT_FAILURE);
    WebServer.start();
    if (&signal != 0) {
        DEBUG_COUT(RED << "Le programme a été interrompu ..." << RESET);
        return (EXIT_FAILURE);
    }
    return (EXIT_SUCCESS);
}
