/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cgi.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 16:19:40 by fassani           #+#    #+#             */
/*   Updated: 2021/06/07 09:49:00 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./Cgi.hpp"
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <cstring>

//https://fr.wikipedia.org/wiki/Variables_d%27environnement_CGI
Env &createCgiEnv(ClientSocket &clientsocket, Env &env)
{
    env.add("REDIRECT_STATUS", "200");

    env.add("AUTH_TYPE", clientsocket.getHttpRequest().getHeaders().getAuthorization().type);
    env.add("CONTENT_LENGTH", to_string(clientsocket.getHttpRequest().getHeaders().getContent_Length()));
    env.add("CONTENT_TYPE", clientsocket.getHttpRequest().getHeaders().getContent_Type().media_type);
    env.add("GATEWAY_INTERFACE", "CGI/1.1");
    env.add("PATH_INFO", clientsocket.getHttpRequest().getHeaders().getRequest().Path);
    env.add("PATH_TRANSLATED", env.get("SCRIPT_FILENAME"));
    env.add("QUERY_STRING", clientsocket.getHttpRequest().getHeaders().getRequest().Query);
    env.add("REMOTE_ADDR", inet_ntoa(clientsocket.getCliAddr().sin_addr));
    env.add("REMOTE_IDENT", clientsocket.getHttpRequest().getHeaders().getAuthorization().credentials);
    env.add("REMOTE_USER", clientsocket.getHttpRequest().getHeaders().getAuthorization().credentials);
    env.add("REQUEST_METHOD", clientsocket.getHttpRequest().getHeaders().getRequest().Method);
    env.add("REQUEST_URI", clientsocket.getHttpRequest().getHeaders().getRequest().Path);
    env.add("SERVER_NAME", HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ())->getServerNames().front());
    env.add("SERVER_PORT", to_string(HttpRequestProcess::getConf(clientsocket.getPort(), clientsocket.getConfigServ())->getPort()));
    env.add("SERVER_PROTOCOL", "HTTP/1.1");
    env.add("SERVER_SOFTWARE", "WebServ/1.0");

    //ajoute les autres headers HTTP_xxx
    std::map<std::string, std::string>::iterator it = clientsocket.getHttpRequest().getHeaders().getMap_header().begin();
    std::map<std::string, std::string>::iterator ite = clientsocket.getHttpRequest().getHeaders().getMap_header().end();

    while (it != ite)
    {
        std::string key = to_string(it->first);
        std::transform(key.begin(), key.end(), key.begin(), ::toupper);
        env.add("HTTP_" + key, to_string(it->second));
        it++;
    }
    env.add("HTTP_HOST", clientsocket.getHttpRequest().getHeaders().getHost().host);

    return (env);
}

std::string Cgi::execute(std::string cgi_path, ClientSocket &clientsocket)
{
    std::cerr << "CGI execution ..." << std::endl;

    //créer l'env
    Env env;
    env.add("SCRIPT_NAME", cgi_path);
    env.add("SCRIPT_FILENAME", clientsocket.getHttpRequest().getResolvedPath());
    env = createCgiEnv(clientsocket, env);

    // std::cout << CYAN << env << RESET << std::endl;

    pid_t pid = 0;
    int status = 0;
    std::string cgi_response_path = "./.tmp/cgi_response_" + generate_random_tmp_path();
    mkdir("./.tmp", 0777);

    pid = fork();
    if (pid == -1)
    {
        std::cerr << "Erreur, fork" << std::endl;
    }
    else if (pid > 0)
    {
        waitpid(pid, &status, WUNTRACED);
        if (WIFEXITED(status))
            std::cerr << strerror(WEXITSTATUS(status)) << std::endl;
    }
    else
    {

        close(1);
        if (open(cgi_response_path.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644) == -1)
        {
            std::cerr << "Erreur,  impossible de créer le fichier tmp : " << cgi_response_path << std::endl;
            exit(0);
        }

        close(0);
        if (open(clientsocket.getHttpRequest().getHeaders().getBody_tmp_path().c_str(), O_RDONLY) == -1)
        {
            std::cout << "Erreur, fichier tmp non trouvé : " << clientsocket.getHttpRequest().getHeaders().getBody_tmp_path() << std::endl;
            // exit(0);
        }

        char **args = NULL;
        if (!(args = static_cast<char **>(malloc(sizeof(char *) * 3))))
            return (0);

        char *buffer = NULL;
        if (!(buffer = static_cast<char *>(malloc(sizeof(char) * (env.get("SCRIPT_NAME").size() + 1)))))
            return (0);

        char *buffer2 = NULL;
        if (!(buffer2 = static_cast<char *>(malloc(sizeof(char) * (env.get("SCRIPT_FILENAME").size() + 1)))))
            return (0);

        size_t i = 0;
        for (; i < env.get("SCRIPT_NAME").size(); i++)
        {
            buffer[i] = env.get("SCRIPT_NAME").at(i);
        }
        buffer[i] = 0;

        size_t y = 0;
        for (; y < env.get("SCRIPT_FILENAME").size(); y++)
        {
            buffer2[y] = env.get("SCRIPT_FILENAME").at(y);
        }
        buffer2[y] = 0;

        (void)cgi_path;

        std::vector<char *> v;
        v.push_back(buffer);
        v.push_back(buffer2);
        v.push_back(0);

        if (execve(v[0], &v[0], env.getEnv()) == -1) {
            std::cerr << "Erreur, CGI" << std::endl;
            // perror("execve");
        }

        if (buffer)
        {
            free(buffer);
            buffer = NULL;
        }

        if (buffer2)
        {
            free(buffer2);
            buffer2 = NULL;
        }

        v.clear();

        // env.clear();
        exit(0);
        // std::cerr << "cgi response tmp créé" << std::endl;
    }
    return (cgi_response_path);
}
