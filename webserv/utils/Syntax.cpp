#include "../Webserv.hpp"

/* Destructor */
Syntax::~Syntax() {}

/* Attributs */
const Syntax::method_tab_entry_t
Syntax::method_tab[] = {
	{GET, "GET"},
	{HEAD, "HEAD"},
	{POST, "POST"},
	{PUT, "PUT"},
	{DELETE, "DELETE"},
	{CONNECT, "CONNECT"},
	{OPTIONS, "OPTIONS"},
	{TRACE, "TRACE"},
	{PATCH, "PATCH"},
};

const Syntax::server_instructions_tab_entry_t
Syntax::server_instructions_tab[] = {
	{LISTEN, "listen"},
	{SERVER_NAME, "server_name"},
	{ERROR_PAGE, "error_page"},
	{LOCATION_INSTRUCTION, "location"}
};

const Syntax::location_instructions_tab_entry_t
Syntax::location_instructions_tab[] = {
	{ROOT, "root"},
	{METHODS, "dav_methods"},
	{INDEX, "index"},
	{CGI, "cgi"},
	{AUTOINDEX, "autoindex"},
	{CLIENT_MAX_BODY_SIZE, "client_max_body_size"},
	{UPLOAD_STORE, "upload_store"},
	{AUTH_BASIC, "auth_basic"},
	{AUTH_BASIC_USER_FILE, "auth_basic_user_file"}
};

const Syntax::status_code_tab_entry_t
Syntax::status_codes_tab[] = {
	{CONTINUE, 100, "100", "Continue"},
	{SWITCHING_PROTOCOLS, 101, "101", "Switching Protocols"},
	{OK, 200, "200", "OK"},
	{CREATED, 201, "201", "Created"},
	{ACCEPTED, 202, "202", "Accepted"},
	{NON_AUTHORITATIVE_INFORMATION, 203, "203", "Non-Authoritative Information"},
	{NO_CONTENT, 204, "204", "No Content"},
	{RESET_CONTENT, 205, "205", "Reset Content"},
	{PARTIAL_CONTENT, 206, "206", "Partial Content"},
	{MULTIPLE_CHOICES, 300, "300", "Multiple Choices"},
	{MOVED_PERMANENTLY, 301, "301", "Moved Permanently"},
	{FOUND, 302, "302", "Found"},
	{SEE_OTHER, 303, "303", "See Other"},
	{NOT_MODIFIED, 304, "304", "Not Modified"},
	{USE_PROXY, 305, "305", "Use Proxy"},
	{TEMPORARY_REDIRECT, 307, "307", "Temporary Redirect"},
	{BAD_REQUEST, 400, "400", "Bad Request"},
	{UNAUTHORIZED, 401, "401", "Unauthorized"},
	{PAYMENT_REQUIRED, 402, "402", "Payment Required"},
	{FORBIDDEN, 403, "403", "Forbidden"},
	{NOT_FOUND, 404, "404", "Not Found"},
	{METHOD_NOT_ALLOWED, 405, "405", "Method Not Allowed"},
	{NOT_ACCEPTABLE, 406, "406", "Not Acceptable"},
	{PROXY_AUTHENTICATION_REQUIRED, 407, "407", "Proxy Authentication Required"},
	{REQUEST_TIMEOUT, 408, "408", "Request Time-out"},
	{CONFLICT, 409, "409", "Conflict"},
	{GONE, 410, "410", "Gone"},
	{LENGTH_REQUIRED, 411, "411", "Length Required"},
	{PRECONDITION_FAILED, 412, "412", "Precondition Failed"},
	{PAYLOAD_TOO_LARGE, 413, "413", "Payload Too Large"},
	{URI_TOO_LONG, 414, "414", "URI Too Long"},
	{UNSUPPORTED_MEDIA_TYPE, 415, "415", "Unsupported Media Type"},
	{RANGE_NOT_SATISFIABLE, 416, "416", "Range Not Satisfiable"},
	{EXPECTATION_FAILED, 417, "417", "Expectation Failed"},
	{UPGRADE_REQUIRED, 426, "426", "Upgrade Required"},
	{INTERNAL_SERVER_ERROR, 500, "500", "Internal Server Error"},
	{NOT_IMPLEMENTED, 501, "501", "Not Implemented"},
	{BAD_GATEWAY, 502, "502", "Bad Gateway"},
	{SERVICE_UNAVAILABLE, 503, "503", "Service Unavailable"},
	{GATEWAY_TIMEOUT, 504, "504", "Gateway Timeout"},
	{HTTP_VERSION_NOT_SUPPORTED, 505, "505", "HTTP Version Not Supported"},
	{TOTAL_STATUS_CODE, 0, "0", ""}
};

bool Syntax::isClientErrorCode(int code) {
	return (code >= 400 && code <= 417) || code == 426;
}

bool Syntax::isServerErrorCode(int code) {
	return code >= 500 && code <= 505;
}

bool Syntax::isErrorCode(int code) {
	return isServerErrorCode(code) || isClientErrorCode(code);
}
