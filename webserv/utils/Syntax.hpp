#ifndef SYNTAX_HPP
#define SYNTAX_HPP

#include "../Webserv.hpp"

enum path_type_t {
	REGULAR_FILE,
	DIRECTORY,
	INVALID_PATH
};

enum method_t {
	GET,
	HEAD,
	POST,
	PUT,
	DELETE,
	CONNECT,
	OPTIONS,
	TRACE,
	PATCH,
	DEFAULT_METHOD,

};

enum status_code_t {
	CONTINUE,
	SWITCHING_PROTOCOLS,
	OK,
	CREATED,
	ACCEPTED,
	NON_AUTHORITATIVE_INFORMATION,
	NO_CONTENT,
	RESET_CONTENT,
	PARTIAL_CONTENT,
	MULTIPLE_CHOICES,
	MOVED_PERMANENTLY,
	FOUND,
	SEE_OTHER,
	NOT_MODIFIED,
	USE_PROXY,
	TEMPORARY_REDIRECT,
	BAD_REQUEST,
	UNAUTHORIZED,
	PAYMENT_REQUIRED,
	FORBIDDEN,
	NOT_FOUND,
	METHOD_NOT_ALLOWED,
	NOT_ACCEPTABLE,
	PROXY_AUTHENTICATION_REQUIRED,
	REQUEST_TIMEOUT,
	CONFLICT,
	GONE,
	LENGTH_REQUIRED,
	PRECONDITION_FAILED,
	PAYLOAD_TOO_LARGE,
	URI_TOO_LONG,
	UNSUPPORTED_MEDIA_TYPE,
	RANGE_NOT_SATISFIABLE,
	EXPECTATION_FAILED,
	UPGRADE_REQUIRED,
	INTERNAL_SERVER_ERROR,
	NOT_IMPLEMENTED,
	BAD_GATEWAY,
	SERVICE_UNAVAILABLE,
	GATEWAY_TIMEOUT,
	HTTP_VERSION_NOT_SUPPORTED,
	TOTAL_STATUS_CODE
};

enum server_instructions_t {
	LISTEN,
	SERVER_NAME,
	ERROR_PAGE,
	LOCATION_INSTRUCTION,
	TOTAL_SERVER_INSTRUCTIONS
};

enum location_instructions_t {
	ROOT,
	METHODS,
	INDEX,
	CGI,
	AUTOINDEX,
	CLIENT_MAX_BODY_SIZE,
	UPLOAD_STORE,
	AUTH_BASIC,
	AUTH_BASIC_USER_FILE,
	TOTAL_LOCATION_INSTRUCTIONS
};

class Syntax
{
public:
    /* Destructor */
    ~Syntax();

    /* Attributs */
	struct method_tab_entry_t {
		method_t		method_index;
		std::string		name;
	};
    static const method_tab_entry_t	method_tab[];

	struct status_code_tab_entry_t {
		status_code_t	code_index;
		int				code_int;
		std::string		code_str;
		std::string		reason_phrase;
	};
	static const status_code_tab_entry_t status_codes_tab[];

	struct location_instructions_tab_entry_t {
		location_instructions_t instruction_index;
		std::string name; 
	};
	static const location_instructions_tab_entry_t location_instructions_tab[];
	
	struct server_instructions_tab_entry_t {
		server_instructions_t instruction_index;
		std::string name; 
	};
	static const server_instructions_tab_entry_t server_instructions_tab[];

	static bool isClientErrorCode(int code);
	static bool isServerErrorCode(int code);
	static bool isErrorCode(int code);

private:
    /* Constructor */
    Syntax();
    /* Copy constructor */
    Syntax(const Syntax& copy);
    /* Overloading operator */
    const Syntax& operator = (const Syntax& assign);

};  // class Syntax

#endif
