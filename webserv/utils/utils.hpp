/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/01 16:47:17 by fassani           #+#    #+#             */
/*   Updated: 2021/06/06 20:16:55 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>

std::vector<std::string> split_string(std::string str, std::string delimiter);
std::string random_string(int size);
std::string generate_random_tmp_path();
std::string resolvePath(const std::string folderPath, const std::string filePath);
std::string get_Path_Extention(std::string path);
int fileExist(std::string path);
char *readBinaryFile(std::string path, std::streampos *size);
void createtmpfile(std::string path);
std::string getPathFileName(std::string path);
std::string getPathFileDir(std::string path);

std::string getCurrentDate(void);
std::string getPathLastModifiedDate(std::string path);

template <typename T>
std::string to_string(const T &t)
{
  std::ostringstream ss;
  ss << t;
  return ss.str();
}

#endif