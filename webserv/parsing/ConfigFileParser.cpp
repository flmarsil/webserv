#include "../Webserv.hpp"

/* Destructor */
ConfigFileParser::~ConfigFileParser() {}

/* Methods */
int ConfigFileParser::checkConfigFile(std::ifstream& configFile, std::string& filepath) {
	size_t extensionPos = filepath.find(".conf");

	if (extensionPos == std::string::npos || extensionPos != filepath.size() - 5) {
		std::cerr << RED"Le fichier '" << filepath << "' n'a pas la bonne extension" << RESET << std::endl;
		return (FAILURE);
	}
	configFile.open(filepath.c_str(), std::ios::in);
	if (!configFile) {
		std::cerr << RED"Le fichier '" << filepath << "' n'a pas pu s'ouvrir" << RESET << std::endl;
		return (FAILURE);
	}
	return (SUCCESS);
}

int ConfigFileParser::parseConfigFile(std::ifstream& configFile, std::list<ConfigServer>& configServer) {
	std::vector<std::string> tokens;

	while (configFile) {
		tokens = lineTokenizer(configFile);

		if (tokens.empty())
			continue ;
		if (tokens[0] != "server")
			return (unknowToken(tokens[0]));
		if (tokens.size() == 1 || (tokens.size() == 2 && tokens[1] != "{"))
			return (errorCurlyBracket(tokens[1], OPENING));
		if (tokens.size() > 2)
			return (invalidNumberArguments(1, tokens.size() - 1, "server"));
		if (checkServerBloc(configFile, configServer) == FAILURE)
			return (FAILURE);
	}
	return (SUCCESS);
}

// Server bloc parsing
int ConfigFileParser::checkServerBloc(std::ifstream& configFile, std::list<ConfigServer>& configServer) {
	std::vector<std::string> tokens;
	ConfigServer serverBloc;
	std::set<server_instructions_t> serverDirectivesFiller;
	ConfigServer::Location serverLocation;  // attributs server present dans location sans etre dans un bloc location 
	std::set<location_instructions_t> serverLocationDirectivesFiller;
	bool bracket_close = false;

	while (configFile) {
		tokens = lineTokenizer(configFile);
		if (tokens.empty())
			continue ;
		if (tokens[0] == "}") {
			if (tokens.size() > 1){
				return (curlyBracketMustToBeAlone(tokens[0]));
			}
			bracket_close = true;
			break ;
		}
		if (tokens[0] == "location") {
			if (tokens.size() != 3)
				return (invalidNumberArguments(3, tokens.size(), tokens[0]));
			std::string path = tokens[1];
			if (path[0] != '/')
				return (errorPathSyntax(path));
			std::string bracket = tokens[2];
			if (bracket != "{")
				return (errorCurlyBracket(bracket, OPENING));
			serverBloc.getLocationTmp().clear();
			// serverBloc.getLocationTmp() = serverLocation;   // copie du bloc location server
			serverBloc.getLocationTmp().setPath(path);
			// std::cout << CYAN << "LocationTmp path = [" << serverBloc.getLocationTmp().getPath() << "]" << RESET << std::endl;
			if (checkLocationBloc(configFile, serverBloc, serverBloc.getLocationTmp()) == FAILURE)
				return (FAILURE);
		}
		else if (checkLocationDirectives(tokens[0]) == true) {
			if (locationDirectivesHandler(tokens, serverLocationDirectivesFiller, serverLocation) == FAILURE)
				return (FAILURE);
		 }
		else if (checkServerDirectives(tokens[0]) == true) {
			if (serverDirectivesHandler(tokens, serverDirectivesFiller, serverBloc) == FAILURE)
				return (FAILURE);
		}
		else
			return (unknowToken(tokens[0]));
	}
	if (bracket_close == false) 
		return (errorCurlyBracket(tokens[0], OPENING));
	if (serverDirectivesFiller.find(LISTEN) == serverDirectivesFiller.end())
		return (serverBlocDirectiveError("listen"));
	if (serverLocationDirectivesFiller.find(ROOT) == serverLocationDirectivesFiller.end())
		return (serverBlocDirectiveError("root"));
	if (serverLocationDirectivesFiller.find(METHODS) == serverLocationDirectivesFiller.end())
		return (serverBlocDirectiveError("dav_methods"));
	if (serverDirectivesFiller.find(ERROR_PAGE) == serverDirectivesFiller.end())
		return (serverBlocDirectiveError("error_page"));
	if (serverDirectivesFiller.find(SERVER_NAME) == serverDirectivesFiller.end())
		return (serverBlocDirectiveError("server_name"));

	serverBloc.addLocationBloc(serverLocation);
	setDefaultLocation(serverBloc.getLocationBlocs());

	configServer.push_back(serverBloc);
	return (SUCCESS);
}

void ConfigFileParser::setDefaultLocation(std::list<ConfigServer::Location>& locationBlocs) {
	ConfigServer::Location& defaultLocation = locationBlocs.back();

	std::list<ConfigServer::Location>::iterator it = locationBlocs.begin();
	std::list<ConfigServer::Location>::iterator ite = --locationBlocs.end();
	for (; it != ite ; ++it) {
		if (it->getRoot().empty())
			it->setRoot(defaultLocation.getRoot());
		if (it->getCGIpath().empty())
			it->setCGIpath(defaultLocation.getCGIpath());
		if (it->getCGIextention().empty())
			it->setCGIextention(defaultLocation.getCGIextention());
		// if (it->getAutoIndex() == false && defaultLocation.getAutoIndex() == true)
		// 	it->setAutoIndex(true);
		if (it->getMethods().empty())
			it->setMethods(defaultLocation.getMethods());
		if (it->getIndex().empty())
			it->setIndex(defaultLocation.getIndex());
		if (it->getClientMaxBodySize() == DEFAULT_MAX_CLIENT_BODY_SIZE && defaultLocation.getClientMaxBodySize() != DEFAULT_MAX_CLIENT_BODY_SIZE)
			it->setClientMaxBodySize(defaultLocation.getClientMaxBodySize());
		if (it->getUploadStore().empty())
			it->setUploadStore(defaultLocation.getUploadStore());
		// voir si ajouter auth basic - auth basic user file
	}
}

bool ConfigFileParser::checkServerDirectives(const std::string& directive) {
	for (size_t i = 0; i < TOTAL_SERVER_INSTRUCTIONS; i++) {
		if (Syntax::server_instructions_tab[i].name == directive) {
			return (true);
		}
	}
	return (false);
}

int ConfigFileParser::serverDirectivesHandler(std::vector<std::string>& tokens, std::set<server_instructions_t>& serverDirectivesFiller, ConfigServer& serverBloc) {
	
	int (*function[TOTAL_SERVER_INSTRUCTIONS])(const std::vector<std::string>& tokens, ConfigServer& serverBloc) = {
		&parseListenDirective,
		&parseServerNameDirective,
		&parseErrorPageDirective,
	};
	
	std::string directive = tokens[0];
	for (size_t i = 0 ; i < TOTAL_SERVER_INSTRUCTIONS ; i++) {
		if (Syntax::server_instructions_tab[i].name == directive) {
			if (serverDirectivesFiller.find(Syntax::server_instructions_tab[i].instruction_index) != serverDirectivesFiller.end())
				return (doubleDirectives(directive));
			if (trimSemiColon(tokens.back()) == FAILURE)
				return (semiColonNotFound(tokens.back()));
			if ((*function[i])(tokens, serverBloc))
				return (FAILURE);
			serverDirectivesFiller.insert(Syntax::server_instructions_tab[i].instruction_index);
			break ;
		}
	}
	return (SUCCESS);
}

int ConfigFileParser::parseListenDirective(const std::vector<std::string>& tokens, ConfigServer& serverBloc) {
	if (tokens.size() != 2)
		return (invalidNumberArguments(1, tokens.size() - 1, tokens[0]));
	
	std::string port;
	size_t colonPos;

	colonPos = tokens[1].find(':');
	if (colonPos != std::string::npos) {
		std::string hostname = tokens[1].substr(0, colonPos);
		if (isNumber(hostname.c_str()) == true) {
			if (checkIpSyntax(hostname) == FAILURE) {
				std::cerr << RED << "L'adresse IP n'est pas correcte : " << hostname << RESET << std::endl;
				return (FAILURE);
			}
		}
		port = tokens[1].substr(colonPos + 1);
		serverBloc.setHostname(hostname);
	}
	else 
		port = tokens[1];

	if (isNumber(port.c_str()) == false)
		return (nonDigitError(tokens[0]));
	
	int portNumber = std::strtol(port.c_str(), NULL, 10);
	if (portNumber <= 0 || portNumber > 65535) {
		std::cerr << RED << "Numéro de port invalide : '" << port << "'. (1 - 65535)" << RESET << std::endl;
		return (FAILURE);
	}
	serverBloc.setPort(portNumber);

	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::cout << GREEN << "Listen port [" << serverBloc.getPort() << "]" << RESET << std::endl;
	// std::cout << GREEN << "Hostname [" << serverBloc.getHostname() << "]" << RESET << std::endl;
	return (SUCCESS);
}

int ConfigFileParser::parseServerNameDirective(const std::vector<std::string>& tokens, ConfigServer& serverBloc) {
	if (tokens.size() < 2)
		return (invalidNumberArguments(1, tokens.size() - 1, tokens[0]));

	std::list<std::string> serverNameList(tokens.begin() + 1, tokens.end());
	serverBloc.setServerNames(serverNameList);

	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::list<std::string>::const_iterator it1 = serverNameList.begin();
	// std::list<std::string>::const_iterator ite1 = serverNameList.end();
	// for (; it1 != ite1 ; ++it1) {
	// 	std::cout << GREEN << "Server name [" << *it1 << "]" << RESET << std::endl;
	// }
	return (SUCCESS);
}

int ConfigFileParser::parseErrorPageDirective(const std::vector<std::string>& tokens, ConfigServer& serverBloc) {
	if (tokens.size() < 3)
		return (invalidNumberArguments(1, tokens.size() - 1, tokens[0]));

	std::list<status_code_t> errorPages;
	std::string errorPath;
	int errorNumber;
	size_t i = 0;

	if (isNumber(tokens.back().c_str()) == true) {
		std::cerr << RED << "'error_page' : le dernier argument doit être un path valide vers le dossier contenant les fichiers de pages d'erreur." << RESET << std::endl;
		return (FAILURE);
	}

	std::vector<std::string>::const_iterator it = tokens.begin() + 1;
	std::vector<std::string>::const_iterator ite = tokens.end() - 1;
	for (; it != ite ; ++it) {
		if (isNumber((*it).c_str()) == false)
			return (nonDigitError(*it));
		errorNumber = std::strtol((*it).c_str(), NULL, 10);
		if (Syntax::isErrorCode(errorNumber) == false) {
			std::cerr << RED << "Argument : " << errorNumber << " n'est pas un code de page erreur valide." << RESET << std::endl;
			return (FAILURE);
		}
		while (Syntax::status_codes_tab[i].code_index != TOTAL_STATUS_CODE
				&& Syntax::status_codes_tab[i].code_int != errorNumber)
				i++;
		errorPages.push_back(Syntax::status_codes_tab[i].code_index);
		i = 0;
	}

	if (pathChecker(tokens.back()) == INVALID_PATH)
		return (errorPathSyntax(tokens[0]));
	errorPath = tokens.back();

	serverBloc.setErrorPageCodes(errorPages);
	serverBloc.setErrorPagePath(errorPath);

	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::list<status_code_t>::const_iterator it1 = errorPages.begin();
	// std::list<status_code_t>::const_iterator ite1 = errorPages.end();
	// for (; it1 != ite1 ; ++it1) {
	// 	std::cout << GREEN << "Error code page [" << *it1 << "]" << RESET << std::endl;
	// }
	// std::cout << GREEN << "Error path [" << serverBloc.getErrorPagePath() << "]" << RESET << std::endl;
	return (SUCCESS);
}

// Location bloc parsing
int ConfigFileParser::checkLocationBloc(std::ifstream& configFile, ConfigServer& serverBloc, ConfigServer::Location& location) {
	std::vector<std::string> tokensLocation;    
	std::set<location_instructions_t> directivesFiller;
	bool bracket_close = false;

	while (configFile) {
		tokensLocation = lineTokenizer(configFile);
		// std::cout << RED << tokensLocation[0] << RESET << std::endl;
		if (tokensLocation.empty())
			continue ;
		if (tokensLocation[0] == "}") {
			if (tokensLocation.size() > 1){
				return (curlyBracketMustToBeAlone(tokensLocation[0]));
			}
			bracket_close = true;
			break ;
		}
		if (checkLocationDirectives(tokensLocation[0]) == true) {
			if (locationDirectivesHandler(tokensLocation, directivesFiller, location) == FAILURE)
				return (FAILURE);
		 }
		else
			return (unknowToken(tokensLocation[0]));
	}
	if (bracket_close == false) 
		return (errorCurlyBracket(tokensLocation[0], OPENING));

	serverBloc.addLocationBloc(location);
	return (SUCCESS);
}

bool ConfigFileParser::checkLocationDirectives(const std::string& directive) {
	for (size_t i = 0; i < TOTAL_LOCATION_INSTRUCTIONS; i++) {
		if (Syntax::location_instructions_tab[i].name == directive)
			return (true);
	}
	return (false);
}

int ConfigFileParser::locationDirectivesHandler(std::vector<std::string>& tokensLocation,
	std::set<location_instructions_t>& directivesFiller, ConfigServer::Location& location) {
	
	int (*function[TOTAL_LOCATION_INSTRUCTIONS])(const std::vector<std::string>&, ConfigServer::Location& location) = {
		&parseRootDirective,
		&parseDavMethodsDirective,
		&parseIndexDirective,
		&parseCgiDirective,
		&parseAutoIndexDirective,
		&parseClientMaxBodySizeDirective,
		&parseUploadStoreDirective,
		&parseAuthBasicDirective,
		&parseAuthBasicUserFileDirective
	};

	std::string directive = tokensLocation[0];
	for (size_t i = 0 ; i < TOTAL_LOCATION_INSTRUCTIONS ; i++) {
		if (Syntax::location_instructions_tab[i].name == directive) {
			if (directivesFiller.find(Syntax::location_instructions_tab[i].instruction_index) != directivesFiller.end())
				return (doubleDirectives(directive));
			if (trimSemiColon(tokensLocation.back()) == FAILURE)
				return (semiColonNotFound(tokensLocation.back()));
			if ((*function[i])(tokensLocation, location))
				return (FAILURE);
			directivesFiller.insert(Syntax::location_instructions_tab[i].instruction_index);
			break ;
		}
	}
	return (SUCCESS);
}

int ConfigFileParser::parseRootDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location) {
	if (tokensLocation.size() != 2)
		return (invalidNumberArguments(1, tokensLocation.size() - 1, tokensLocation[0]));
	std::string path = tokensLocation[1];
	if (pathChecker(path) == INVALID_PATH)
		return (errorPathSyntax(path));
	location.setRoot(path);

	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::cout << GREEN << "root path [" << location.getRoot() << "]" << RESET << std::endl;
	return (SUCCESS);
}

int ConfigFileParser::parseDavMethodsDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location) {
	std::vector<std::string>::const_iterator it = tokensLocation.begin() + 1;
	std::vector<std::string>::const_iterator ite = tokensLocation.end();
	std::set<std::string> methodFiller;
	int ret;

	for (; it != ite ; ++it) {
		if ((ret = methodChecker(*it)) && ret == -1)
			return (errorMethod(*it, "INVALID"));
		else {
			if (methodFiller.find(Syntax::method_tab[ret].name) != methodFiller.end())
				return (errorMethod(*it, "MULTI"));
			methodFiller.insert(Syntax::method_tab[ret].name);
		}
	}

	std::list<std::string> methods (tokensLocation.begin() + 1, tokensLocation.end());
	location.setMethods(methods);

	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::list<std::string>::const_iterator it1 = methods.begin();
	// std::list<std::string>::const_iterator ite1 = methods.end();
	// for (; it1 != ite1 ; ++it1) {
	// 	std::cout << GREEN << "dav_method [" << *it1 << "]" << RESET << std::endl;
	// }
	return (SUCCESS);
}

int ConfigFileParser::parseIndexDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location) {
	if (tokensLocation.size() < 2) 
		return (invalidNumberArguments(1, tokensLocation.size() - 1, tokensLocation[0]));

	std::list<std::string> indexList(tokensLocation.begin() + 1, tokensLocation.end());
	location.setIndex(indexList);

	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::list<std::string>::const_iterator it1 = indexList.begin();
	// std::list<std::string>::const_iterator ite1 = indexList.end();
	// for (; it1 != ite1 ; ++it1) {
	// 	std::cout << GREEN << "index [" << *it1 << "]" << RESET << std::endl;
	// }
	return (SUCCESS);
}

int ConfigFileParser::parseCgiDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location) {
	if (tokensLocation.size() != 3)
		return (invalidNumberArguments(2, tokensLocation.size() - 1, tokensLocation[0]));

	std::string extension = tokensLocation[1];
	std::string path = tokensLocation[2];

	if (extension.size() < 3 || (extension.size() > 2 && (extension[0] != '*' || extension[1] != '.')))
		return (errorCGI(tokensLocation[0]));

	if (pathChecker(path) == INVALID_PATH)
		return (errorPathSyntax(tokensLocation[0]));
	
	location.setCGIextention(extension);
	location.setCGIpath(path);

	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// 	std::cout << GREEN << "cgi extension [" << location.getCGIextension() << "]" << RESET << std::endl;
	// std::cout << GREEN << "cgi path [" << location.getCGIpath() << "]" << RESET << std::endl;
	return (SUCCESS);
}

int ConfigFileParser::parseAutoIndexDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location) {
	if (tokensLocation.size() != 2)
		return (invalidNumberArguments(1, tokensLocation.size() - 1, tokensLocation[0]));

	std::string param = tokensLocation[1];
	if (param == "on")
		location.setAutoIndex("on");
	else if (param != "on" && param != "off") {
		std::cerr << RED << "`autoindex' : " << param << " n 'est pas correct. (on/off)" << RESET << std::endl;
		return (FAILURE);
	}
	
	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::cout << GREEN << "Autoindex [" << location.getAutoIndex() << "]" << RESET << std::endl;
	return (SUCCESS);
}

int ConfigFileParser::parseClientMaxBodySizeDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location) {
	if (tokensLocation.size() != 2) 
		return (invalidNumberArguments(1, tokensLocation.size() - 1, tokensLocation[0]));

	if (isNumber(tokensLocation[1].c_str()) == false)
		return (nonDigitError(tokensLocation[0]));

	unsigned long maxClientBodySize = std::strtol(tokensLocation[1].c_str(), NULL, 10);
	if (maxClientBodySize == 0) {
		std::cerr << RED << "`client_max_body_size' ne peut pas être nul." << RESET << std::endl;
		return (FAILURE);
	}
	location.setClientMaxBodySize(maxClientBodySize);
	
	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::cout << GREEN << "ClientMaxBodySize [" << location.getClientMaxBodySize() << "]" << RESET << std::endl;
	return (SUCCESS);
}

int ConfigFileParser::parseUploadStoreDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location) {
	if (tokensLocation.size() != 2)
		return (invalidNumberArguments(1, tokensLocation.size() - 1, tokensLocation[0]));
	
	std::string path = tokensLocation[1];
	if (pathChecker(path) == INVALID_PATH)
		return (errorPathSyntax(path));

	location.setUploadStore(path);

	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::cout << GREEN << "UploadStore [" << location.getUploadStore() << "]" << RESET << std::endl;
	return (SUCCESS);
}

int ConfigFileParser::parseAuthBasicDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location) {
	if (tokensLocation.size() < 2)
		return (invalidNumberArguments(1, tokensLocation.size() - 1, tokensLocation[0]));

	std::string firstWord = tokensLocation[1];
	if (firstWord != "off") {
		std::string lastWord = tokensLocation.back();
		if (trimQuotation(firstWord, lastWord) == FAILURE)
			return (noQuotationError(tokensLocation[0]));
		
		std::vector<std::string>::const_iterator it = tokensLocation.begin() + 2;
		std::vector<std::string>::const_iterator ite = tokensLocation.end() - 1;
		for (; it != ite ; ++it) {
			// ajouter if dans *it find " return error
			firstWord += " " + *it;
		}
		firstWord += " " + lastWord;
		location.setAuthBasic(firstWord);
	}
	
	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::cout << GREEN << "authBasic [" << location.getAuthBasic() << "]" << RESET << std::endl;
	return (SUCCESS);
}

int ConfigFileParser::parseAuthBasicUserFileDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location) {
	if (tokensLocation.size() != 2)
		return (invalidNumberArguments(1, tokensLocation.size() - 1, tokensLocation[0]));
	
	std::string path = tokensLocation[1];
	if (pathChecker(path) == INVALID_PATH)
		return (errorPathSyntax(path));

	location.setAuthBasicUserFile(path);

	/* AFFICHAGE DANS LE TERMINAL POUR DEBUG */
	// std::cout << GREEN << "authBasicUserFile [" << location.getAuthBasicUserFile() << "]" << RESET << std::endl;
	return (SUCCESS);
}

// Error messages
int ConfigFileParser::serverBlocDirectiveError(const std::string& directive) {
	std::cerr << RED << "L'instruction : '" << directive << "' n'est pas présente dans le bloc server." << RESET << std::endl;
	return (EXIT_FAILURE);
}
int ConfigFileParser::errorCGI(const std::string& token) {
	std::cerr << RED << "Erreur " << token << ". Les extensions CGI doivent être indiqué comme suit '*.extension'" << RESET << std::endl;
	return (EXIT_FAILURE);
}
int ConfigFileParser::noQuotationError(const std::string& directive) {
	std::cerr << RED << "L'argument de la directive: " << directive << " doit être 'off' ou une string compris entre deux '\"'." << RESET << std::endl;
	return (EXIT_FAILURE);
}

int ConfigFileParser::nonDigitError(const std::string& directive) {
	std::cerr << RED << "L'argument de la directive: " << directive << " doit être un nombre." << RESET << std::endl;
	return (EXIT_FAILURE);
}

int ConfigFileParser::errorMethod(const std::string& method, const std::string& status) {
	if (status == "INVALID")
		std::cerr  << RED << "La commande "<< "`" << method << "' n'est pas une commande valide." << RESET << std::endl;
	else if (status == "MULTI")
		std::cerr  << RED << "La commande "<< "`" << method << "' apparait plusieurs fois : aucun doublon autorisé." << RESET << std::endl;
	return (EXIT_FAILURE);
}

int ConfigFileParser::doubleDirectives(const std::string& directive) {
	std::cerr << RED << "Aucun doublon de directive n'est autorisé au sein d'un même bloc : " << directive << RESET << std::endl;
	return (EXIT_FAILURE);
}

int ConfigFileParser::semiColonNotFound(const std::string& lastCell) {
	std::cerr << RED << "Chaque directive doit se terminer par un semi colon ';'. Voir '" << lastCell << "'." << RESET << std::endl;
	return (EXIT_FAILURE);
}
int ConfigFileParser::curlyBracketMustToBeAlone(const std::string& token) {
	std::cerr << RED << "L'instruction : '" << token << "' doit être suivit d'un retour à la ligne" << RESET << std::endl;
	return (EXIT_FAILURE);
}

int ConfigFileParser::errorPathSyntax(const std::string& token) {
	std::cerr << RED << "L'instruction path : '" << token << "' n'est pas correcte." << RESET << std::endl;
	return (EXIT_FAILURE);
}

int ConfigFileParser::unknowToken(const std::string& token) {
	std::cerr << RED << "L'instruction : '" << token << "' n'est pas reconnue." << RESET << std::endl;
	return (EXIT_FAILURE);
}

int ConfigFileParser::errorCurlyBracket(const std::string& token, const curly_bracket_t& bracket){
	std::string str = (bracket == OPENING) ? "ouverte" : "fermée";
	std::cerr << RED << "L'instruction : '" << token << "' n'est pas correcte" << std::endl;
	std::cerr << "Instruction attendue : bracket '" << str << "'"  << RESET << std::endl;
	return (EXIT_FAILURE);
}

int ConfigFileParser::invalidNumberArguments(int expected, int found, const std::string& instruction) {
	std::cerr  << RED << "`" << instruction << "' instruction a besoin de " << expected <<" argument(s) ";
	std::cerr << "et " << found << " a/ont été trouvé." << RESET << std::endl;
	return (EXIT_FAILURE);
}

// Utils
std::vector<std::string> ConfigFileParser::lineTokenizer(std::ifstream& configFile) {
	std::string lineBuffer;

	std::getline(configFile, lineBuffer);
	lineBuffer = trimComments(lineBuffer);
	lineBuffer = trimWhiteSpaces(lineBuffer);

	std::vector<std::string> result;
	const char* ws = WHITESPACES;
	char* tk;

	tk = strtok(&lineBuffer[0], ws);
	while (tk) {
		result.push_back(tk);
		tk = strtok(NULL, ws);
	}
	return (result);
}

std::string ConfigFileParser::trimComments(std::string& line) {
	std::string newLine = line;
	size_t hashPos;

	hashPos = newLine.find('#');
	if (hashPos == std::string::npos)
		return newLine;
	return (newLine.substr(0, hashPos));
}

std::string ConfigFileParser::trimWhiteSpaces(std::string& line) {
	std::string newLine = line;
	const char* ws = WHITESPACES;

	newLine.erase(newLine.find_last_not_of(ws) + 1);
	newLine.erase(0, newLine.find_first_not_of(ws));
	return (newLine);
}

int ConfigFileParser::trimSemiColon(std::string& lastCell) {
	if (lastCell.at(lastCell.size() - 1) != ';')
		return (FAILURE);
	else 
		lastCell.resize(lastCell.size() - 1);
	return (SUCCESS);
}

path_type_t ConfigFileParser::pathChecker(const std::string& path) {
	struct stat buffer;
	int ret;
	//  stat() récupère l'état du fichier pointé par path et remplit le tampon buffer. 
	// voir http://manpagesfr.free.fr/man/man2/stat.2.html
	ret = stat(path.c_str(), &buffer);
	if (ret == -1)
		return (INVALID_PATH);
	// S_ISDIR vérifie s'il s'agit d'un répertoire
	return ((S_ISDIR(buffer.st_mode) ? DIRECTORY : REGULAR_FILE));
}

int ConfigFileParser::methodChecker(const std::string& method) {
	for (size_t i = 0 ; i < DEFAULT_METHOD ; i++)
		if (method == Syntax::method_tab[i].name)
			return (i);
	return (-1);
}

bool ConfigFileParser::isNumber(const char* bodySize) {
	for (size_t i = 0 ; bodySize[i] ; i++) {
		if (bodySize[i] == '.')
			continue;
		else if (std::isdigit(bodySize[i]) == false)
			return (false);
	}
	return (true);
}

int ConfigFileParser::trimQuotation(std::string& firstWord, std::string& lastWord) {
	if (firstWord.at(0) == '"' && lastWord.at(lastWord.size() - 1) == '"') {
		firstWord.erase(0, firstWord.find_first_not_of('"'));
		lastWord.resize(lastWord.size() - 1);
		return (SUCCESS);
	}
	return (FAILURE);
}

int ConfigFileParser::checkIpSyntax(const std::string& hostname) {
	std::string ipAddr = hostname;
	std::vector<std::string> bytes;
	int value;
	char* tk;

	tk = strtok(&ipAddr[0], ".");
	while (tk) {
		bytes.push_back(tk);
		tk = strtok(NULL, ".");
	}

	if (bytes.size() != 4)
		return (FAILURE);

	bool ret ;
	for (int i = 0 ; i < 4 ; i++) {
		ret = isNumber(bytes[i].c_str());
		if (ret == false)
			return (FAILURE);
		value = std::strtol(bytes[i].c_str(), NULL, 10);
		if (value < 0 || value > 255)
			return (FAILURE);
	}
	return (SUCCESS);
}