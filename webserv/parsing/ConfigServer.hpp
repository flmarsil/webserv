#ifndef ConfigServer_hpp
#define ConfigServer_hpp

#include "../Webserv.hpp"

/*
    ConfigServer :

    Contient toutes les informations nécessaires à l'instantiation du serveur.
    Elles sont indiqués dans le fichier de configuration passé en paramètre
    du programme. 

*/

#define DEFAULT_MAX_CLIENT_BODY_SIZE 1048576 		// nginx default

class ConfigServer
{
public:
    /* Aliases definition  */
    typedef int                   		PORT;
    typedef std::list<std::string>      list_s;
	
	/* Attributs */
    class Location
    {
    public:
        /* Constructor */
        Location();
        /* Destructor */
        ~Location();
        /* Copy constructor */
        Location(const Location& copy);
        /* Overloading operator */
        const Location& operator = (const Location& assign);
        /* Methods */
        void clear();
        // Setters
        void setPath(const std::string& path);
        void setRoot(const std::string& root);
        void setMethods(const std::list<std::string>& methods);
        void setIndex(const std::list<std::string>& index);
        void setClientMaxBodySize(unsigned long size);
        void setAutoIndex(bool index);
        void setUploadStore(const std::string& store);
        void setCGIextention(const std::string& extension);
        // void setCGIextention(const std::list<std::string>& extension);
        void setCGIpath(const std::string& path);
		void setAuthBasic(const std::string& message);
		void setAuthBasicUserFile(const std::string& path);

        // Getters
		std::string& getPath();
		std::string& getRoot();
		list_s& getMethods();
        list_s& getIndex();
        std::string& getUploadStore();
        // list_s& getCGIextention();
        std::string& getCGIextention();
        std::string& getCGIpath();
		std::string& getAuthBasic();
		std::string& getAuthBasicUserFile();

        const std::string& getPath() const;
        const std::string& getRoot() const;
        const list_s& getMethods() const;
        const list_s& getIndex() const;
        unsigned long getClientMaxBodySize() const;
        bool getAutoIndex() const;
        const std::string& getUploadStore() const;
        const std::string& getCGIextention() const;
        // const list_s& getCGIextention() const;
        const std::string& getCGIpath() const;
		const std::string& getAuthBasic() const;
		const std::string& getAuthBasicUserFile() const;

    private:
        /* Attributs */
        std::string     _path;
        std::string     _root;
        list_s          _davMethods;
        list_s          _index;
        unsigned long   _clientMaxBodySize;
        bool            _autoIndex;
        std::string     _uploadStore;
        std::string     _CGIextention;
        // list_s          _CGIextention;
        std::string     _CGIpath;
		std::string     _authBasic; 		// message a afficher dans la fenetre de connexion
    	std::string     _authBasicUserFile; // path vers le fichier htpasswd contenant les id/pwd users

    }; // class Location

public:
    /* Constructor */
    ConfigServer();
    /* Destructor */
    ~ConfigServer();
    /* Copy constructor */
    ConfigServer(const ConfigServer& copy);
    /* Overloading operator */
    const ConfigServer& operator = (const ConfigServer& assign);
    /* Methods */
	void addLocationBloc(const Location& bloc);
    static std::list<const ConfigServer*> configServerListCreator(const std::list<ConfigServer>& configServers, int serverPort);

    // Setters 
	void setPort(PORT port);
    void setHostname(const std::string& hostname);
	void setServerNames(const std::list<std::string>& serverNames);
	void setErrorPagePath(const std::string& path);
	void setErrorPageCodes(const std::list<status_code_t>& statusCodes);

    // Getters
	PORT getPort() const;
    const std::string& getHostname() const;
    std::string getHostname();
	const list_s& getServerNames() const;
	const std::string& getErrorPagePath() const;
	const std::list<status_code_t>& getErrorPageCodes() const;
    const std::list<Location>& getLocationBlocs() const;
	std::list<Location>& getLocationBlocs();
	Location& getLocationTmp();

private:
    /* Attributs */
    PORT						_listen;
    std::string                 _hostname;
    list_s						_serverNames;
    std::string     			_errorPagePath;
	std::list<status_code_t>	_errorPageCodes;
    
    std::list<Location>     	_locationBlocs;
	Location 					_locationTmp;
}; // class ConfigServer

#endif
