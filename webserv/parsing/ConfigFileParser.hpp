#ifndef CONFIGFILEPARSER_HPP
#define CONFIGFILEPARSER_HPP

#include "../Webserv.hpp"
/*
    ConfigParser :

    Prend en input un fichier de configuration serveur, le parse, enregistre les 
    données utiles pour la création du serveur HTTP.
    Il permettra ensuite de lancer le constructeur du serveur web avec les données 
    de configuration enregistrées.
*/

enum curly_bracket_t {
    OPENING,
    CLOSING,
    NONE
};

class ConfigFileParser
{
public:
    /* Destructor */
    ~ConfigFileParser();

    /* Methods */
    static int checkConfigFile(std::ifstream& configFile, std::string& filepath);
    static int parseConfigFile(std::ifstream& configFile, std::list<ConfigServer>& configServer);

    // Server parsing 
    static int checkServerBloc (std::ifstream& configFile, std::list<ConfigServer>& configServer);
    static bool checkServerDirectives(const std::string& directive);
    static int serverDirectivesHandler(std::vector<std::string>& tokens, std::set<server_instructions_t>& serverDirectivesFiller, ConfigServer& serverBloc);
    static int parseListenDirective(const std::vector<std::string>& tokens, ConfigServer& serverBloc);
    static int parseServerNameDirective(const std::vector<std::string>& tokens, ConfigServer& serverBloc);
    static int parseErrorPageDirective(const std::vector<std::string>& tokens, ConfigServer& serverBloc);
    static void setDefaultLocation(std::list<ConfigServer::Location>& locationBlocs);

    // Location parsing
    static int checkLocationBloc(std::ifstream& configFile, ConfigServer& serverBloc, ConfigServer::Location& location);
    static bool checkLocationDirectives(const std::string& directive);
    static int locationDirectivesHandler(std::vector<std::string>& tokensLocation, std::set<location_instructions_t>& directivesFiller, ConfigServer::Location& location);
    static int parseRootDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location);
    static int parseDavMethodsDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location);
    static int parseIndexDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location);
    static int parseCgiDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location);
    static int parseAutoIndexDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location);
    static int parseClientMaxBodySizeDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location);
    static int parseUploadStoreDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location);
    static int parseAuthBasicDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location);
    static int parseAuthBasicUserFileDirective(const std::vector<std::string>& tokensLocation, ConfigServer::Location& location);

    // Error messages
    static int serverBlocDirectiveError(const std::string& directive);
    static int errorCGI(const std::string& token);
    static int semiColonNotFound(const std::string& lastCell);
    static int unknowToken(const std::string& token);
    static int errorCurlyBracket(const std::string& token, const curly_bracket_t& bracket);
    static int errorPathSyntax(const std::string& token);
    static int invalidNumberArguments(int expected, int found, const std::string& instruction);
    static int curlyBracketMustToBeAlone(const std::string& token);
    static int doubleDirectives(const std::string& directive);
    static int errorMethod(const std::string& method, const std::string& status);
    static int nonDigitError(const std::string& directive);
    static int noQuotationError(const std::string& directive);

    // Utils 
    static int trimQuotation(std::string& firstWord, std::string& lastWord);
    static int trimSemiColon(std::string& lastCell);
    static std::string trimComments(std::string& line);
    static std::string trimWhiteSpaces(std::string& line);
    static std::vector<std::string> lineTokenizer(std::ifstream& configFile);
    static path_type_t pathChecker(const std::string& path);
    static int methodChecker(const std::string& method);
    static bool isNumber(const char* bodySize);
    static int checkIpSyntax(const std::string& hostname);

private:
    /* Constructor */
    ConfigFileParser();
};  // class ConfigFileParser

#endif
