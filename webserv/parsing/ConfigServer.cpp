#include "../Webserv.hpp"

/* Constructor */
ConfigServer::ConfigServer() {}

ConfigServer::Location::Location()
    :   _clientMaxBodySize(DEFAULT_MAX_CLIENT_BODY_SIZE),
        _autoIndex(false),
        _authBasic("off") {}

/* Destructor */
ConfigServer::~ConfigServer() {}

ConfigServer::Location::~Location() {}

/* Copy constructor */
ConfigServer::ConfigServer(const ConfigServer& copy)
    :   _listen(copy._listen),
        _hostname(copy._hostname),
        _serverNames(copy._serverNames),
        _errorPagePath(copy._errorPagePath) {
        _errorPageCodes.assign(copy._errorPageCodes.begin(), copy._errorPageCodes.end());
        _locationBlocs.assign(copy._locationBlocs.begin(), copy._locationBlocs.end());
}

ConfigServer::Location::Location(const Location& copy)
    :   _path(copy._path),
        _root(copy._root),
        _davMethods(copy._davMethods),
        _index(copy._index),
        _clientMaxBodySize(copy._clientMaxBodySize),
        _autoIndex(copy._autoIndex),
        _uploadStore(copy._uploadStore),
        _CGIextention(copy._CGIextention),
        _CGIpath(copy._CGIpath),
        _authBasic(copy._authBasic),
        _authBasicUserFile(copy._authBasicUserFile) {}

/* Overloading operator */
const ConfigServer& ConfigServer::operator = (const ConfigServer& assign) {
    _listen = assign.getPort();
    _hostname = assign.getHostname();
    _serverNames = assign.getServerNames();
    _errorPagePath = assign.getErrorPagePath();
    _errorPageCodes = assign.getErrorPageCodes();
    _locationBlocs = assign.getLocationBlocs();
    return *this;
}

const ConfigServer::Location& ConfigServer::Location::operator = (const ConfigServer::Location& assign) {
    _path = assign.getPath();
    _root = assign.getRoot();
    _davMethods = assign.getMethods();
    _index = assign.getIndex();
    _clientMaxBodySize = assign.getClientMaxBodySize();
    _autoIndex = assign.getAutoIndex();
    _uploadStore = assign.getUploadStore();
    _CGIextention = assign.getCGIextention();
    _CGIpath = assign.getCGIpath();
    _authBasic = assign.getAuthBasic();
    _authBasicUserFile = assign.getAuthBasicUserFile();
    return *this;
}

/* Methods */

// Selectionne parmis tous les blocs serveurs du configfile, uniquement les blocs serveurs correspondants aux demandes du clients socket
// permet normalement a un client socket de communiquer avec differents blocs serveur et donc d'attribuer une liste de configserv sur mesure
std::list<const ConfigServer*> ConfigServer::configServerListCreator(const std::list<ConfigServer>& configServers, int serverPort) {

        std::list<const ConfigServer*> ret;

        std::list<ConfigServer>::const_iterator it = configServers.begin();
        std::list<ConfigServer>::const_iterator ite = configServers.end();
        for (; it != ite ; ++it) {
            if (serverPort == it->getPort()) {
                ret.push_back(it.operator->());
            }
        }
    return (ret);
}

void ConfigServer::addLocationBloc(const ConfigServer::Location& bloc) {
    _locationBlocs.push_back(bloc);
}

void ConfigServer::Location::clear() {
    getPath().clear();
    getRoot().clear();
    getMethods().clear();
    getIndex().clear();
    getUploadStore().clear();
    getCGIextention().clear();
    getCGIpath().clear();
    setAuthBasic("off");
    getAuthBasicUserFile().clear();
    setClientMaxBodySize(DEFAULT_MAX_CLIENT_BODY_SIZE);
    setAutoIndex(false);
}

// ConfigServer getters
ConfigServer::PORT ConfigServer::getPort() const { return _listen; }
const std::string& ConfigServer::getHostname() const { return _hostname; }
std::string ConfigServer::getHostname() { return _hostname; }
const ConfigServer::list_s& ConfigServer::getServerNames() const { return _serverNames; }
const std::string& ConfigServer::getErrorPagePath() const { return _errorPagePath; }
const std::list<status_code_t>& ConfigServer::getErrorPageCodes() const { return _errorPageCodes; }
const std::list<ConfigServer::Location>& ConfigServer::getLocationBlocs() const { return _locationBlocs; }
std::list<ConfigServer::Location>& ConfigServer::getLocationBlocs() { return _locationBlocs; }
ConfigServer::Location& ConfigServer::getLocationTmp() { return _locationTmp; }

// Location getters
std::string& ConfigServer::Location::getPath() { return _path; }
std::string& ConfigServer::Location::getRoot() { return _root; }
ConfigServer::list_s& ConfigServer::Location::getMethods() { return _davMethods; }
ConfigServer::list_s& ConfigServer::Location::getIndex() { return _index; }
std::string& ConfigServer::Location::getUploadStore() { return _uploadStore; }
// ConfigServer::list_s& ConfigServer::Location::getCGIextention() { return _CGIextention; }
std::string& ConfigServer::Location::getCGIextention() { return _CGIextention; }
std::string& ConfigServer::Location::getCGIpath() { return _CGIpath; }
std::string& ConfigServer::Location::getAuthBasic() { return _authBasic; }
std::string& ConfigServer::Location::getAuthBasicUserFile() { return _authBasicUserFile; }

const std::string& ConfigServer::Location::getPath() const { return _path; }
const std::string& ConfigServer::Location::getRoot() const { return _root; }
const ConfigServer::list_s& ConfigServer::Location::getMethods() const { return _davMethods; }
const ConfigServer::list_s& ConfigServer::Location::getIndex() const { return _index; }
unsigned long ConfigServer::Location::getClientMaxBodySize() const { return _clientMaxBodySize; }
bool ConfigServer::Location::getAutoIndex() const { return _autoIndex; }
const std::string& ConfigServer::Location::getUploadStore() const { return _uploadStore; }
const std::string& ConfigServer::Location::getCGIextention() const { return _CGIextention; }
// const ConfigServer::list_s& ConfigServer::Location::getCGIextention() const { return _CGIextention; }
const std::string& ConfigServer::Location::getCGIpath() const { return _CGIpath; }
const std::string& ConfigServer::Location::getAuthBasic() const { return _authBasic; }
const std::string& ConfigServer::Location::getAuthBasicUserFile() const { return _authBasicUserFile; }

// ConfigServer Setters
void ConfigServer::setPort(PORT port) { _listen = port; }
void ConfigServer::setHostname(const std::string& hostname) { _hostname = hostname; }
void ConfigServer::setServerNames(const std::list<std::string>& serverNames) { _serverNames = serverNames; }
void ConfigServer::setErrorPagePath(const std::string& path) { _errorPagePath = path; }
void ConfigServer::setErrorPageCodes(const std::list<status_code_t>& statusCodes) { _errorPageCodes = statusCodes; }

// Location setters
void ConfigServer::Location::setPath(const std::string& path) { _path = path; }
void ConfigServer::Location::setRoot(const std::string& root) { _root = root; }
void ConfigServer::Location::setMethods(const std::list<std::string>& methods) { _davMethods = methods; }
void ConfigServer::Location::setIndex(const std::list<std::string>& index) { _index = index; }
void ConfigServer::Location::setClientMaxBodySize(unsigned long size) { _clientMaxBodySize = size; }
void ConfigServer::Location::setAutoIndex(bool index) { _autoIndex = index; }
void ConfigServer::Location::setUploadStore(const std::string& store) { _uploadStore = store; }
void ConfigServer::Location::setCGIextention(const std::string& extension) { _CGIextention = extension; }
// void ConfigServer::Location::setCGIextention(const ConfigServer::list_s& extension) { _CGIextention = extension; }
void ConfigServer::Location::setCGIpath(const std::string& path) { _CGIpath = path; }
void ConfigServer::Location::setAuthBasic(const std::string& message) { _authBasic = message; }
void ConfigServer::Location::setAuthBasicUserFile(const std::string& path) { _authBasicUserFile = path; }
